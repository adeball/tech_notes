* Install

MacOS: brew install graphviz seems to do the trick, but X11 is not picked up
Linux: yum install graphviz (or apt-get)

* Display from CLI

MacOS
dot -Tpng < test.dot > /tmp/a.png ; open /tmp/a.png

Linux
dot -Tx11 < test.dot 


Examples at: http://graphs.grevian.org
