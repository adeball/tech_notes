## Dell OpenManage - PERC controller

Utils in /opt/dell/srvadmin/bin

Web interface at https://host:1311/  (root)

Finickity syntax, so here are some working examples

### Create RAID

        omconfig storage controller action=createvdisk controller=0 raid=r5 pdisk=1:0:5,1:0:6,1:0:7 size=max

### Remove RAID

        omconfig storage vdisk action=deletevdisk controller=0 vdisk=2

### Reconfigure

	omconfig storage vdisk action=reconfigure controller=0 vdisk=2 raid=r6 pdisk=1:0:5,1:0:6,1:0:7,0:0:1,0:0:2,0:0:3

### Report examples

	 omreport storage globalinfo
	 omreport storage pdisk controller=0 vdisk=2
	 omreport storage pdisk | grep %
	 omreport storage pdisk pdisk=1:0:4 controller=0
	 omreport storage vdisk controller=0
	 omreport storage vdisk controller=0 | grep %
	 omreport storage vdisk controller=0 vdisk=0
	 omreport storage vdisk | grep %
	 omreport system alertlog

### Dump from history, not all are valid

Just some sample command syntax for reference.

	 omconfig storage action=deletevdisk controller=0 vdisk=1
	 omconfig storage controller action=createvdisk controller=0 raid=r0 pdisk=0:0:1,0:0:2,0:0:3
	 omconfig storage controller controller=0 action=createvdisk raid=r0 size=max pdisk=0:0:2
	 omconfig storage controller controller=0 action=createvdisk raid=r1 size=max pdisk=1:0:5
	 omconfig storage pdisk action=assignhotspare controller=0 pdisk=1:0:5 assign=no
	 omconfig storage pdisk vdisk=1 action=assigndedicatedhotspare controller=0 pdisk=1:0:5 assign=no
	 omconfig storage vdisk=1 action=assigndedicatedhotspare controller=0 pdisk=1:0:5 assign=no
	 omconfig storage vdisk action=assigndedicatedhotspare controller=0 pdisk=1:0:5 assign=no
	 omconfig storage vdisk action=assignhotspare controller=0 pdisk=1:0:5 assign=no
	 omconfig storage vdisk action=createvdisk controller=0 raid=r0 pdisk=0:0:1,0:0:2,0:0:3
	 omconfig storage vdisk action=deletevdisk controller=0 vdisk=2
	 omconfig storage vdisk action=reconfigure controller=0 vdisk=2 raid=r1 pdisk=0:0:1,0:0:2,0:0:3,1:0:5,1:0:6,1:0:7
	 omconfig storage vdisk action=reconfigure controller=0 vdisk=2 raid=r1 pdisk=1:0:5,0:0:1
	 omconfig storage vdisk controller=0 action=reconfigure vdisk=2 raid=10 pdisk=0:0:1,0:0:2,0:0:3
	 omconfig storage vdisk controller=0 action=reconfigure vdisk=2 raid=r10 pdisk=0:0:1,0:0:2,0:0:3
	 omconfig storage vdisk controller=0 action=reconfigure vdisk=2 raid=r10 pdisk=0:0:1,0:0:2,0:0:3,1:0:5,1:0:6,1:0:7
	 omconfig storage vdisk controller=0 pdisk=0:0:2 vdisk=1 raid=r0
	 omconfig storage vdisk controller=0 pdisk=0:0:2 vdisk=1 raid=r0 action=initialize
	 omconfig storage vdisk controller=0 pdisk=1:0:6,1:0:7 raid=r0 vdcapacityexpansion=yes action=reconfigure
	 omconfig storage vdisk controller=0 pdisk=1:0:6,1:0:7 vdisk=2 raid=r0 vdcapacityexpansion=yes action=reconfigure
	 omconfig storage vdisk vdisk=1 action=assigndedicatedhotspare controller=0 pdisk=1:0:5 assign=no


