
After the leap second update, systems running particular processes (the Dell monitoring daemon _dsm_om_connsvcd_ 
was specifically noted as problematic) jumped to high %system CPU usage at midnight.

Red Hat's advice is to reboot, but there's an easier fix :-)

Check base %system usage (e.g.) sar -f /var/log/sa/*yesterday's-sa-file*
Then use: date -s "`date -u`"  [No idea *why* this works as it's just setting the time to what it already is, but it does!]

(https://access.redhat.com/articles/15145)

