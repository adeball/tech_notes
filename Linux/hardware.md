
## Disk

lsblk
Usual LVM stuff: pvs, vgs, lvs, *scan, *display etc

## General (very useful!)

dmidecode
lspci
lscpu (more concise than /proc/cpuinfo), shows NUMA etc

## Memory
### Check DIMMs

dmidecode

### RAM Usage

free (-m)

#### /proc/meminfo
Committed_AS = RAM that the kernel calculates it can commit (if progs use it, you are in trouble).

Check for:-
* over committing (https://www.kernel.org/doc/Documentation/vm/overcommit-accounting) - might need to prevent over committing altogether if the application profile demands it
* cache not being released (echo 3 > /proc/sys/vm/drop_caches)
* cache not released quickly enough (e.g. vfs_cache_pressure)
