
### Where RPM packages are clashing...

###### Select interactive vmware tools installer (right click guest etc in vcenter)

    mount /dev/cdrom /mnt
    cd /tmp
    tar xvfz /mnt/VMwareTools*
    umount /mnt 
    rpm -qa | sed -n '/vmware/ s/-[0-9].*$//p' | xargs yum -y erase
    cd vmware-tools-distrib
    ./vmware-install.pl --default 
    
###### Check vcenter now reports VMware tools are current

* In vcenter, edit the guest settings, Options/VMware Tools/Advanced - tick "Check and upgrade Tools during power cycling"
* (Note: The virtual ISO for the tools should be unmounted already, but check)

###### Check services will autostart

The latest version doesn't seem to put the start/stop script in place, so you may need to do something like this:-
NB: The script isn't chkconfig compliant, so you need to create the links yourself.

    cp installer/services.sh /etc/init.d/vmtools
    cd /etc/rc3.d ; ln -s ../init.d/vmtools S99vmtools
    cd /etc/rc2.d ; ln -s ../init.d/vmtools K01vmtools

