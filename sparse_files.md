
# Dealing with sparse files

## tar

* Backup with e.g. tar cvfzSp (where the 'S' is the flag to store sparse files correctly)
* Restore does not need the flag, the file will be restored correctly.

## rsync

* Use e.g. rsync -avxz --sparse  (for new copies)
* Use e.g. rsync -avxz --inplace (to update changed blocks for existing copies


## cp

* Use cp --sparse

