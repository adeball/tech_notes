
## Network stop/start

Uses network-manager, so
service network-manager stop/start/restart etc (not network or networking)


## DNS

To add DNS servers/search domains, add something like this to /etc/network/interfaces:-


iface eth0 inet dhcp
    dns-nameservers 1.2.3.4 1.2.3.5
    dns-search this.domain.com that.domain.com

