

# Install stuff (as root)
'''
apt install net-tools nfs-common keepass2 snap gnome-tweak-tool rclone
snap install spotify
'''

### Get .ssh/id_rsa from github (web interface), then clone tech_notes
### & private repos
###
### echo ". ~/tech_notes/bashrc >> ~/.bashrc"
### cd ; rm -rf .ssh ; ln -s ~/private/.ssh .ssh


# Set up DLNA audio
## As root (or sudo)
'''
add-apt-repository ppa:qos/pulseaudio-dlna
apt-get update
apt-get install pulseaudio-dlna
'''

## As user
pulseaudio-dlna


# Google Drive

rclone config (Use MCC account)
mkdir MCC
e.g. rclone sync MCC:Competitions MCC/Competitions

