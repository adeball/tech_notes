
Before installing Vbox additions
(You often get misleading errors WRT kernel libs missing etc, when it's actually the compiler and/or
make which is not available).

# Ubuntu 14.x

apt-get -y install build-essential linux-headers-`uname -r` linux-headers-generic dkms
apt-get -y install gcc make
#echo "options snd-intel8x0 ac97_clock=48000" >> /etc/modprobe.d/alsa-base.conf

# Centos 6.8

yum install -y kernel-devel gcc make
