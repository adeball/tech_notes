### Spacewalk 2.2 notes

Installing this from scratch with no prior experience…

NB: Spacewalk 2.2, searching the web for problems found often led down “documentation black holes” with out-of-date information for older versions.

Basic steps for future reference:-

Install a clean CentOS 6.5 server instance on VirtualBox. eth0 set up connected to the local internal network, with the host system providing DNS (spacewalk.dev.hp5.co.uk 192.168.56.11). eth1 set up as bridged interface, connected to the outside world.
Add 192.168.56.1 (host system) to /etc/resolv.conf.
Add PEERDNS=no to /etc/sysconfig/network-scripts to stop /etc/resolv.conf being over-written.
Follow the installation instructions at: https://fedorahosted.org/spacewalk/wiki/HowToInstall
Connect and create user account as per instructions.
Create default activation key
Create channel for CentOS6.5
Mount CentOS6.5 image on /mnt
cp /var/www/html/pub/RHN-ORG-TRUSTED-SSL-CERT /usr/share/rhn
rhnpush –channel=centos6.5-x86_64 –server=http://localhost –dir=/mnt/Packages

TBC…
