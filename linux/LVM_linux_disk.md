### Scan host0 (check for others)
	echo "- - -" > /sys/class/scsi_host/host0/scan

### If this fails, you can use (e.g. for sda)
echo "1" > /sys/class/block/sda/device/rescan


### You cannot resize an active partition - needs a reboot.
* If expanding e.g. vgopsys or equiv, add another partition instead.  e.g. create /dev/sda3
* Use e.g. partx -a -v /dev/sda to add a new partition online 
* pvcreate /dev/sda3


### Multipath race condition

e.g. on IBM S390 instances - filter on relevant multipath devs, otherwise e.g. /dev/sda gets read by LVM and only a single path is used.

Edit etc/lvm/lvm.conf at or around line 146 add below line: 
filter =  [ "a|/dev/dasda2|", "a|/dev/mapper/mpath|", "r|.*|" ]
