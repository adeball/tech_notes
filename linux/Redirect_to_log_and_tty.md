
###### Bash

	#!/bin/bash

	exec > >(tee logfile.txt)
	exec 2>&1


###### Ksh


	#!/bin/ksh

	### Direct all output to logfile and terminal
	### Not as easy as in bash!
	### Lifted from http://www.unix.com/shell-programming-and-scripting/85584-redirect-within-ksh.html


	### Start script

	exec 3>&1 4>&2
	FIFO=/tmp/fifo.$$
	[[ -e $FIFO ]] || mkfifo $FIFO
	tee -a logfile < $FIFO >&3 &
	PID=$!
	exec > $FIFO 2>&1



	### SCRIPT HERE



	### End script
	exec 1>&3 2>&4 3>&- 4>&-
	wait $PID
	rm $FIFO

