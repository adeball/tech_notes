### Show aging

	chage -l user

### Force new password at next login

	chage -d 0 user

### Set to never expire (system + application accounts)

	chage -m 0 -M 99999 -I -1 -E -1 root
	-m : min days between password change
	-M : max days between password change
	-I : inactive (-1 for never)
	-E : account expires (-1 for never)
