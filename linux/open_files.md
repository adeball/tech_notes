
If you know the process id: lsof -p <pid>

If not: lsof -nP | grep '(deleted)', or better lsof -nP +L1 (shows files with < 1 link)

You can manipulate the file under /proc/<pid>/fd/<fd>  (where <fd> is the file descriptor)

