##  For SAS disks.

Download from: https://www.dell.com/support/home/en-uk/drivers/driversdetails?driverid=f48c2
This is an rpm, which deploys to /opt/MegaRAID/perccli

Examples

./perccli64 show ctrlcount (shows number of controllers)

./perccli64 /c0 show all (shows everything on controller c0)

./perccli64 /c0/dall show (shows diskgroups on controller c0)

./perccli64 /c0/e32/s8 insert dg=1 array=0 row=6 (Insert a disk into a RAID group - see output from above for controller, enclosure, slot, disk group, array and row numbers)

 ./perccli64 /c0/e32/s8 start rebuild





