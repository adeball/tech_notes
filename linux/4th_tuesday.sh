#!/bin/bash
#
# Note: date +%u is the day of the week number
#

dayofweek=2  	# Tuesday
occurrence=4 	# 4th time in the month

for i in {1..36} ; do 

	m=$(date -d "+$i month" +%m) 
	y=$(date -d "+$i month" +%Y)

	### Three processes (hard-coded to 4th tuesday)
	d1=$(cal -m $m $y | cut -c 4-5 | grep '^[ 0-9][0-9]' | sed -n '4p') 

	### One process (hard-coded to 4th tuesday)
	d2=$(cal -m $m $y |sed -n '/^...[ 0-9][0-9]/{n;n;n;{s/^...\(..\).*/\1/p}}')

	### Using only date calculations
	sday=$(date -d $y-$m-01 +%u) ; [[ $sday -le $dayofweek ]] && sday=$(( $sday + 7 ))
	d3=$(( 7 * $occurrence + 1 + $dayofweek - $sday  ))

	### Vince's one process awk (incorrect when 30 days in the month and it starts on Wednesday)
	d4=$(cal -m $m $y | awk 'NF>4{a=$2} END{print a}')

	### JQ's two process sed
	d5=$(cal -m $m $y | sed -r 's/...//;s/(.{2}).*/\1/;/[0-9]+/!d' | sed -e '4!d')

	echo $y-$m: $d1, $d2, $d3, $d4, $d5

done

