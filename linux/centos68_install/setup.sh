#!/bin/bash
#
# monolithic stuff for very basic setup

echo "export http_proxy=proxy.sdc.hp.com:8080 ; export https_proxy=$http_proxy" \
	> /etc/profile.d/proxy.sh

cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DEVICE=eth0
Type=Ethernet
ONBOOT=yes
NM_CONTROLLED=no
BOOTPROTO=dhcp
EOF

chkconfig NetworkManager off
service NetworkManager stop

service network restart

yum update -y
yum install -y kernel-devel gcc make xterm git ksh nc nmap htop mysql mysql-server

### Add VBox extensions if using virtualbox

useradd -m -c "Ade" -u 3002705 -p '$6$LLrxvAQM$G0eiOhXQ7bb2c01.EULNLQ6ClP6jDCEPDnvVfE9b7jixY15rcZRtnEakaSih8Ho3SxftPOijIL8gBs2LzcLZp.' balladr

####
exit
####


#### Then, as balladr
Create .ssh/id_rsa (go to github website, private repo, cut/paste)
git clone ssh://git@github.com/adeball/tech_notes.git
git clone ssh://git@github.com/adeball/private.git
mv .ssh .ssh.old
ln -s private/.ssh
chown -R 700 .ssh

### Install Dropbox (see tech_notes/dropbox.md for CLI)

yum install -y epel-release
cd /etc/yum.repos.d
wget http://pkgrepo.linuxtech.net/el6/release/linuxtech.repo
