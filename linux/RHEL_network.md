
# Example script to configure bonded and VLAN tagged interfaces on RHEL 5 & 6

This also has pointers WRT to finding and unloading the kernel
modules (so that dodgy MAC addresses get reset with the same values
udevd uses (i.e. what's set up at boot)).



```
#!/bin/ksh
#
# $Id: 040_setup_networking.sh,v 1.4 2016/06/15 10:42:13 root Exp $
#
# Ade Ball (ade@ux1.co.uk)
#


cd $(dirname $0) 
. ./library.ksh

### Check if we are on an NFS mount, if so, copy this script (and the library)
### locally and rerun

df -l . >/dev/null 2>&1
if [[ $? -ne 0 ]] ; then
        echo "I'm not on a local filesystem - copying to /tmp and re-running"
        cp ./library.ksh $0 /tmp || fail "Cannot copy to /tmp"
        exec /tmp/$0
fi

echo "I'm on a local fs, so ok to run..."


PROD1=eth0
PROD2=eth1
PRODVLAN=522
PRODBOND=bond0

BUR=eth2


### Linux can get network configs messed up quite easily, so reset to initial
### values - this will stop/start the interfaces and reinstate the discovered
### MAC addresses

ethmodules=$(for i in $PROD1 $PROD2 $BUR ; do
        ls -l /sys/class/net/$i/device/driver/module | awk -F/ '{print $NF}'
done | sort -u)

print "Unloading bonding & $ethmodules kernel modules"
rmmod bonding $ethmodules

print "Kicking udevd to reload"
udevadm trigger


while read host role prodip prodmask prodbits gw burip burmask burbits asset serial ; do

        if [[ "$host" == "$(hostname -s)" ]] ; then
        
                ### Set up $PRODBOND with VLAN tagging

                print "Setting up $PRODBOND..."

                cat > /etc/sysconfig/network-scripts/ifcfg-$PRODBOND <<- EOF
                ### $PRODBOND, configured by $(pwd)/$0 $date
                DEVICE=$PRODBOND
                BOOTPROTO=none
                ONBOOT=yes
                USERCTL=no
                BONDING_OPTS="mode=1 miimon=500"
                EOF

                print "Setting up $PRODBOND.$PRODVLAN : $prodip/$prodmask"
                cat > /etc/sysconfig/network-scripts/ifcfg-$PRODBOND.${PRODVLAN} <<- EOF
                ### $PRODBOND.$PRODVLAN, configured by $(pwd)/$0 $date
                DEVICE=$PRODBOND.$PRODVLAN
                BOOTPROTO=none
                ONBOOT=yes
                NETMASK=$prodmask
                IPADDR=$prodip
                USERCTL=no
                VLAN=yes
                EOF



                ### Set up prod interfaces as a bonded pair

                print "Setting up $PROD1 & $PROD2 as slaves for $PRODBOND"
                for i in $PROD1 $PROD2 ; do
                        MAC=$(cat /sys/class/net/$i/address)
                        cat > /etc/sysconfig/network-scripts/ifcfg-$i <<- EOF
                        ### $i (prod), configured by $(pwd)/$0 $date
                        DEVICE=$i
                        HWADDR=$MAC
                        ONBOOT=yes
                        BOOTPROTO=none
                        USERCTL=no
                        MASTER=$PRODBOND
                        SLAVE=yes
                        EOF
                done


                ### Set up backup/restore (BUR) interface

                print "Setting up $BUR : $burip/$burmask"
                MAC=$(cat /sys/class/net/$BUR/address)
                cat > /etc/sysconfig/network-scripts/ifcfg-$BUR <<- EOF
                ### $BUR (backup/restore), configured by $(pwd)/$0 $date
                DEVICE=$BUR
                HWADDR=$MAC
                ONBOOT=yes
                BOOTPROTO=none
                NETMASK=$burmask
                IPADDR=$burip
                USERCTL=no
                EOF


        fi

done < etx_hosts

service network restart

```
