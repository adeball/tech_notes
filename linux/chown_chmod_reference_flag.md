###### Very useful

Not sure if this is a GNU-only thing, but worth knowing about

	chmod --reference file1 file2

Changes perms (works with chown too) of file2 to those of file1

Handy as in (e.g.)

	#!/bin/ksh

	find . | grep -v tar$ | while read f ; do
		chmod --reference "$f" /./"$f"
		chown --reference "$f" /./"$f"
	done

