
e.g.

# To list
smbclient -L //ukinstall01/ -U balla -W ukcorplan


# to mount
mount -t cifs //10.10.7.111/Install /tmp/a -o ro,user=balla,workgroup=ukcorplan,pass=mypassword

NB: Use the IP address (yeah I know, makes no sense!).
NB2: Don't use pass=.... unless it's a one off, use credentials=/path/to/creds.smb (or whatever), the file should have two lines, "username=whatever" and "password=whatever" - it's in clear text, so make sure only root can read it.

