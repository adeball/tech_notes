
# Focus follows mouse

As the user: 
gconftool-2 --type string --set /apps/metacity/general/focus_mode mouse


# Smaller titlebars

~/.config/gtk-3.0/gtk.css

window.ssd headerbar.titlebar {
    padding-top: 2px;
    padding-bottom: 2px;
    min-height: 0;
}
window.ssd headerbar.titlebar button.titlebutton {
    padding: 1px;
    min-height: 0;
    min-width: 0;
}

