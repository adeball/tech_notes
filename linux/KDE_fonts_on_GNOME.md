###### Situation:-
Operational server build with minimal install, required 'kate' editor.
'kate' runs, but has squares for the font, many many kde/qt/font packages to choose from,
not much help on the Internet...

###### Fix:-
Install the liberation suite of fonts (on RHEL6, it's as follows):-

    yum -y install liberation-fonts-common liberation-mono-fonts liberation-sans-fonts liberation-serif-fonts
