

### Basic

Add to /etc/exports
e.g.
/home 192.168.99.0/24(rw,no_root_squash)

chkconfig nfs on ; service nfs start
exportfs -a

check with 
showmount -e # (locally)

Make sure rpc is running (probably is by default)

### Firewall

For RPC connection errors from remote, check iptables for TCP/UDP port 111
Use: system-config-firewall for a quick look - though it is fairly useless
Quick fix: Use something like 
-A INPUT -s 192.168.99.0/24 -j ACCEPT
in the filter tables (/etc/sysconfig/iptables) - if you get down to individual ports, it can get 
very messy (see http://www.cyberciti.biz/faq/centos-fedora-rhel-iptables-open-nfs-server-ports/ e.g.)

### NFSv4

Set the domain in /etc/idmapd.conf



