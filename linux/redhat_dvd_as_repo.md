# Use a mounted RHEL CD/DVD as a yum repository

e.g.

* mount the media on /mnt or wherever
* Copy /mnt/media.repo to /etc/yum/repos.d (NB: You need the media ID, which if you want to do this manually, can be found in /mnt/.discinfo)

This is the basic stuff needed

```
[repository] 
mediaid=media_id 
name=repository_name
baseurl=repository_url
gpgkey=gpg_key 
enabled=1 
gpgcheck=1
```

So e.g.
```
[rhel5-Server] 
mediaid=1354216429.587870 
name=RHEL5-Server
baseurl=file:///mnt/rhel5/Server 
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release 
enabled=1 
gpgcheck=1
```

(Or you can set gpgcheck to 0)
