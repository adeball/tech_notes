#!/bin/ksh
#
# $Id: apply_quotas.ksh,v 1.4 2015/07/22 13:48:50 root Exp $
#
# setquota has no option for defaults (strange, but true)
# Therefore users newly creating files in /home or /data 
# will have no quotas applied.
# Run this to set up initially (then use edquota or setquota as required)
#
# Ade <ade@ux1.co.uk> - 2015-07-22
#

### Associative array
typeset -A fs

### NB: The following values are for a physical server, edit for VMs
###     with smaller filesystems.
###
### filesystem, soft quota, hard quota, soft inode quota, hard inode quota
###

fs=(
        [/home]="  2G   3G 0 0" 
        [/data]="190G 200G 0 0"
)

print "\nApplying quotas...\n"
### Iterate filesystems
for i in "${!fs[@]}" ; do

        ### Find unique uids owning files on that filesystem
        for uid in $(find "$i" -exec stat -c '%u' {} \; | sort -u) ; do

                ### Set the quota
                printf "%-15s %-20s %s\n" $i $uid "${fs[$i]}"
                setquota -u $uid ${fs[$i]} $i

        done
done

### And display the results
print "\n"
repquota -a
print
