
http://www.dropboxwiki.com/tips-and-tricks/install-dropbox-centos-gui-required

Basically, as a normal user:-

'''
cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
~/.dropbox-dist/dropboxd
'''

Then note the link to visit and open in a browser
If a proxy needs setting (for some reason http_proxy is ignored):
Leave dropboxd running, and issue (e.g.):-
dropbox proxy manual http proxy.sdc.hp.com 8080
