
### Cloned VMs not able to run nsrexecd

nsrexecd exits as soon as it is started on cloned VMs.

* Remove /nsr/res/nsrladb directory
* Restart nsrexecd (service networker start or whatever)
* The directory should be recreated, and nsrexecd should run.
