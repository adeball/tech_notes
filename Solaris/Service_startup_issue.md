
### Solaris 10 service startup strangeness

Another note for future reference…

Test/dev cluster issues after a power outage – one node would not start RAC/CRS.

I don’t know what the cause was, but the default milestone had switched to “multi-user-server” rather than “all”. This sounds like it should be ok, but it means weird things happen in terms of service start ups.

The clue to look for (when I eventually found out what it was doing) is the console-login service failing to run with the error “Console login service(s) cannot run”, and it dropping back to single-user (this happened in both cluster and non-cluster modes).

If you think you’re seeing the same problem, check the default milestone with:
svccfg -s svc:/system/svc/restarter:default listprop
Look for options/milestone – it should either be set to “all” or not exist. If set to anything else (e.g. multi-user-server), set it to “all” with:-
svcadm milestone -d all

This entry was posted on Wednesday, April 9th, 2014 at 4:11 pm and is filed under Business. You can follow any responses to this entry through the RSS 2.0 feed. Both comments and pings are currently closed.
