#!/usr/bin/ksh
#
# $Id$
#
# Functions:-
#
# backup		: Copies a file to timestamped version
# fail <error>    	: Exit 1 with <error> message
# yn <prompt> [y|n]	: Asks "prompt y/n", with optional default as parameter 2
# make_menu "$1" "$2" ..: $1 is the title, $2 onwards are the options
#
# Works on Solaris / ksh , YMMV elsewhere...
# Should probably tweak some env checks
#


backup()
{

	if [[ -f "$1" ]] ; then
		oldf="$1"
		newf="$1".$(date +"%Y%m%d-%H%M%S")
		echo "Backing up $oldf as $newf"
		cp "$oldf" "$newf"
	fi

}

fail()
{
	echo "An error occurred: $@" >&2
	exit 1
}


yn()
{
	typeset -l ans
	typeset -l default
	prompt="$1"
	default="$2"
	ans="xxx"

	if [[ "$default" == "y" || "$default" == "n" ]] ; then 
		prompt="$prompt (y/n)? [$default]: "
	else
		prompt="$prompt (y/n)? : "
	fi

	while [[ "$ans" != "y" && "$ans" != "n" ]] ; do

		[[ "$ans" != "xxx" ]] && echo "Please answer with y or n"
		read ans?"$prompt"
		if [[ -z "$ans" && -n "$default" ]] ; then
			ans="$default"
		fi
	done

	echo $ans
}


### Writes a numbered menu based on string arguments and returns the
### result as a status code.

make_menu()
{
        printf "\n$1\n\n"; shift
        i=0
        while [[ $i -lt $# ]] ; do
                let i+=1
                eval text="\${$i}"
                printf "\t%2s. %s\n" $i "$text"
        done


        ans=-1
	echo
        while [[ $ans -lt 0 || $ans -gt $i ]] ; do
                [[ $ans -ne -1 ]] && printf "That's an invalid answer - "
                read ans?"Please select 1-$i [<return> to exit]: "
        done

        return $ans

}


### Network/netmask stuff
### NB: To use these, source the file with all the functions, as
### they use each other

NETMASKS=/etc/netmasks


name_to_ip()
{

### $1=hostname
### Outputs IP address
	
	[[ $# -ne 1 ]] && fail "name_to_ip function should take one argument" 

	line=$(getent hosts $1);[[ $? -ne 0 ]] && fail "Cannot resolve hostname ($1)"
	echo $line | awk '{print $1}'
}



ip_mask_to_net()
{

### Inputs: IP_address Netmask
### Output: Network address

	ip_rhs=$1; mask_rhs=$2; i=1; net=""
	while [[ $i -le 4 ]] ; do
		ip_lhs=${ip_rhs%%.*};ip_rhs=${ip_rhs#*.}
		mask_lhs=${mask_rhs%%.*};mask_rhs=${mask_rhs#*.}
		net=${net}$(($ip_lhs&$mask_lhs)).
		let i+=1
	done
	echo ${net%.}
}



ip_to_mask()
{

### Input: IP address
### Output: Netmask 
### Checks $NETMASKS file first - then reverts to standard class A, B or C 

	if [[ -f $NETMASKS ]] ; then
		grep ^[0-9] $NETMASKS | while read net mask ; do
			### Work out IP address masked with netmask, and see if
			### it matches the network
			match=$(ip_mask_to_net $1 $mask)
			if [[ "$match" == "$net" ]] ; then
				echo $mask
				return
			fi
		done 
	else
		echo "No netmasks file: $NETMASKS" >&2
	fi

	echo "No matching netmasks entry, using a standard mask." >&2
	o1=${1%%.*}
	### Class A, B & C
	[[ $o1 -lt 128 ]]                && echo "255.0.0.0"
	[[ $o1 -lt 192 && $o1 -ge 128 ]] && echo "255.255.0.0"
	[[ $o1 -lt 224 && $o1 -ge 192 ]] && echo "255.255.255.0"
}



ip_to_net()
{

### Input: IP address
### Output: Network number

	ip_mask_to_net $(ip_to_mask $1) $1

}	



bits_to_mask()
{

### Input: bits for netmask (0-32)
### Output: Netmask in dotted quad format

	[[ $1 -lt 0 || $1 -gt 32 ]] && fail \
		"Invalid number of bits specified for netmask ($1) - should be 0-32"

	echo $1 | awk '
	{
	    n=$1 ; j=0
	    while (j<=3) { 
		i=7; m[j]=0
		while (i>=0) {
		    if (n>0) { m[j]+=2^i ; n-=1 }
		    i-=1
		}
		j+=1	
	    }
	    printf "%s.%s.%s.%s\n",m[0],m[1],m[2],m[3]
	}'
}


net_info()
{

### Input: IP address/bits (e.g 192.168.185.253/27)
### Output: (Formatted) IP address, netmask, network number

	echo $1 | egrep "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/[0-9]+$" > /dev/null
	[[ $? -ne 0 ]] && fail "Usage: net_info ip_address/bits"

	ip=${1%/*} ; bits=${1#*/}
	mask=$(bits_to_mask $bits)
	network=$(ip_mask_to_net $ip $mask)
	
	echo    "IP:		$ip"
	echo    "Bits in mask:	$bits"	
	echo    "Netmask:	$mask"
	echo    "Network:	$network"
			

}


