### Solaris, OpenFiler, iSCSI, mpxio

This is not a guide, just some notes for my own future reference. If it helps someone else out, so much the better.

The aim:-

A proof of concept exercise (hence not needing a valid production configuration). Configure two Solaris zones with a shared QFS filesystem (ultimately for use by Tibco/EMS to remove the need for Sun Cluster, which is purely there for the global filesystem).

The setup:-

OpenFiler (running on a VirtualBox instance) to emulate production iSCSI (which may be from NetApp, EMC or HP EVA).
Two LUNs configured on the openfiler server.
Two iSCSI targets configured on the OpenFiler, each with paths to both LUNs.
Sun/Oracle T2000 split into two logical domains.
mpxio to provide multipathing to the devices.
IPMP for network resilience (won’t note the config here, it’s well-documented elsewhere!)

Implementation on the Solaris LDOMs:-

    iscsiadm add discovery-address IP-of-OpenFiler
    iscsiadm list discovery-address -v IP-of-OpenFiler – note the target names
    iscsiadm add static-config TARGET1,IP-of-OpenFiler:3260 (where 3260 is the TCP port used, 3260 is the default)
    iscsiadm add static-config TARGET2,IP-of-OpenFiler:3260
    iscsiadm modify discovery –sendtargets enable

That gets us four paths to two devices (see format output to check), so multipathing needs switching on.

    To support OpenFiler, /kernel/drv/scsi_vhci.conf needs a line adding as follows: device-type-scsi-options-list = “OPNFILER”, “symmetric-option”; Also uncomment the line: symmetric-option = 0x1000000;
    Switch on mpxio if not already on: mpxio-disable=”no” in /kernel/drv/fp.conf and /kernel/drv/iscsi.conf
    Reconfig reboot to bring everything into action;
    Check for multipathed devices in format

That’s it for now, will update when (if) I get QFS working. If that works as expected, it will then need configuring in local zones, so details will follow for these notes.

Useful reference: http://docs.oracle.com/cd/E19574-01/817-4091-10/chapter5.html
