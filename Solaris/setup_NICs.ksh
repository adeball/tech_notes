#!/bin/ksh
#
# $Id: install,v 1.1 2012/12/17 13:56:28 root Exp $
#

### Note 2015-07-17: This from the solaris build system, but really just copied
### to github to see the IPMP config example.

. $(dirname $0)/../../../MASTER

[[ "$(zonename)" != "global" ]] && bombout "This should only be run in the global zone"

### Backup and clear existing configuration
cd /etc
for f in $(ls hostname.* dhcp.*) ; do
	backup $f
done
rm -f /etc/hostname.* /etc/dhcp.* 2> /dev/null

netfile=$(mydir)/network
[[ -f "$netfile" ]] || bombout "Cannot determine network configuration file, check $ETC/hosts structure"

echo "Configuring network files on $(hostname), using $netfile"

### Create temp file with IPMP entries at the end
tmpnetfile=/tmp/setup_NICs.$$
grep -v IPMP $netfile > $tmpnetfile
grep IPMP $netfile >> $tmpnetfile


while read host interface net ip ; do

	if [[ "$host" == "${host%#*}" && -n "$host" ]] ; then

		echo "Configuring $host/$interface..."

		### Creates /etc/hostname.*, including basic ones for the test interfaces
		echo "$interface" | egrep "IPMP|ILOM" >/dev/null
		if [[ $? -ne 0 ]] ; then
			echo "$host" > /etc/hostname.$interface	
		fi

		### This overwrites test interfaces with proper IPMP config
		echo "$interface" | egrep "IPMP" > /dev/null
		if [[ $? -eq 0 ]] ; then
			echo "Configuring IPMP "
			echo "$interface" | awk -F: '{print $2, $3}' | read nic1 nic2
			echo "nic1=$nic1, nic2=$nic2"
			echo "$host group $net netmask + broadcast + up addif $(cat /etc/hostname.$nic1) netmask + broadcast + -failover deprecated up" > /etc/hostname.$nic1
			echo "$(cat /etc/hostname.$nic2) netmask + broadcast + group $net deprecated -failover standby up" > /etc/hostname.$nic2
		fi
	fi

done < $tmpnetfile


### Set up /etc/inet/hosts with hostnames for the server/zone in question

add_to_hosts()
{

	host="$1"
	ip="$2"
	comment="$3"

        ### Check hosts file for server entry, add if needed
        getent hosts $host >/dev/null 2>&1
        if [[ $? -ne 0 ]] ; then
                printf "%-15s %-30s %s\n" \
                        "$ip" \
                        "$host" \
                        "# $comment" \
                        >> /etc/inet/hosts
        fi

        getent hosts $host 2>/dev/null
        if [[ $? -ne 0 ]] ; then
                bombout "Error - can't resolve $host"
        fi

}

backup /etc/inet/hosts
cat > /etc/inet/hosts << EOF
#
# /etc/inet/hosts
#

127.0.0.1       localhost                      # Loopback interface

EOF

add_to_hosts "localhost" "127.0.0.1" "Loopback interface"


while read host interface net ip ; do

	if [[ "$host" == "${host%#*}" && -n "$host" ]] ; then

		if [[ "$host" == "$(hostname)" ]] ; then

			add_to_hosts "$host loghost ${host}." "$ip" "$(mydescription)"
		else
			add_to_hosts "$host" "$ip" ""

		fi

	fi

done < $tmpnetfile

echo >> /etc/inet/hosts

### This is a server so add the zones' IPs

if [[ "$(myparent)" == "none" && "$(myzones)" != "none" ]] ; then

	for zone in $(myzones) ; do
		znetfile=$(mydir $zone)/network
		while read host interface net ip ; do
			if [[ "$host" == "${host%#*}" && -n "$host" ]] ; then
				descr="$(mydescription $host)"
				[[ "$host" != "$zone" ]] && descr=""
				add_to_hosts "$host" "$ip" "$descr"
			fi
		done < $znetfile	
	done
	echo >> /etc/inet/hosts
fi


### Add all server hosts

mysite=$(mysite); myenv=$(myenv)
while read type site env rest ; do 

	[[ "$site" == "$mysite" && "$env" == "$myenv" ]] && add_server_to_hosts $type

done < $ETC/servers

rm $tmpnetfile

echo "mgmtrouter" > /etc/defaultrouter
