### Some scripts for reference - used these to set up a new Clariion (in 2006, so it's a bit long in the tooth!)

172.17.0.5 is one of the Clariion service processor addresses.
host_a, host_b etc are the servers connected to the SAN.

#### mk_rgs

Makes RAID groups

	#!/bin/ksh
	#
	# Makes all raid groups
	#

	NAVICLI="/opt/Navisphere/bin/navicli -h 172.17.0.5"

	### Buses 0 to 3 and Enclosures 0 and 1
	for e in 1 0 ; do
		for b in 3 2 1 0 ; do
			rg5=$(($b * 2 - 1 + $e * 8))
			rg9=$(($rg5 + 1))
			echo "b=$b, e=$e, rg5=$rg5, rg9=$rg9"
			if [[ $rg5 != -1 ]] ; then
				echo "Making RAID group $rg5..."
				echo $NAVICLI createrg $rg5 -rm no -pri medium ${b}_${e}_0 ${b}_${e}_1 ${b}_${e}_2 ${b}_${e}_3 ${b}_${e}_4
				$NAVICLI createrg $rg5 -rm no -pri medium ${b}_${e}_0 ${b}_${e}_1 ${b}_${e}_2 ${b}_${e}_3 ${b}_${e}_4
			fi
			echo "Making RAID group $rg9..."
			echo $NAVICLI createrg $rg9 -rm no -pri medium ${b}_${e}_5 ${b}_${e}_6 ${b}_${e}_7 ${b}_${e}_8 ${b}_${e}_9 ${b}_${e}_10 ${b}_${e}_11 ${b}_${e}_12 ${b}_${e}_13
			$NAVICLI createrg $rg9 -rm no -pri medium ${b}_${e}_5 ${b}_${e}_6 ${b}_${e}_7 ${b}_${e}_8 ${b}_${e}_9 ${b}_${e}_10 ${b}_${e}_11 ${b}_${e}_12 ${b}_${e}_13
		done
	done

#### mk_luns

Make LUNs

	#!/bin/ksh
	#
	# Makes all raid groups
	#

	NAVICLI="/opt/Navisphere/bin/navicli -h 172.17.0.5"

	sp1=a

	for rgbase in 0 2 4 6 8 10 12 14 ;do

		rg=$(($rgbase - 1))
		lun=$(($rgbase/2*3-1))
		if [[ $rg -ne -1 ]] ; then
			echo $NAVICLI bind r5 $lun -rg $rg -cap 547419 -sq mb -sp $sp1
			$NAVICLI bind r5 $lun -rg $rg -cap 547419 -sq mb -sp $sp1
			[[ "$sp1" == "a" ]] && sp1=b || sp1=a
		fi
		echo $NAVICLI bind r5 $(($lun+1)) -rg $rgbase -cap 547419 -sq mb -sp a
		$NAVICLI bind r5 $(($lun+1)) -rg $rgbase -cap 547419 -sq mb -sp a

		echo $NAVICLI bind r5 $(($lun+2)) -rg $rgbase -cap 547419 -sq mb -sp b
		$NAVICLI bind r5 $(($lun+2)) -rg $rgbase -cap 547419 -sq mb -sp b

	done

#### rm_luns

Remove LUNs

	#!/bin/ksh
	#
	# Removes all raid groups
	#

	NAVICLI="/opt/Navisphere/bin/navicli -h 172.17.0.5"

	i=0

	while [[ $i -lt 25 ]] ; do
		echo y | $NAVICLI unbind $i
		i=$(($i+1))
	done

#### rm_rgs

Remove RAID groups

	#!/bin/ksh
	#
	# Destroys all raid groups
	#

	NAVICLI="/opt/Navisphere/bin/navicli -h 172.17.0.5"

	i=0

	while [[ $i -lt 15 ]] ; do
		echo "Removing RAID group $i..."
		$NAVICLI removerg $i
		i=$(($i+1))
	done

#### add_hlus

	#!/bin/ksh

	NAVICLI="/opt/Navisphere/bin/navicli -h 172.17.0.5"

	$NAVICLI storagegroup -addhlu -gname SG_host_a -hlu 0 -alu 0
	$NAVICLI storagegroup -addhlu -gname SG_host_a -hlu 1 -alu 1
	$NAVICLI storagegroup -addhlu -gname SG_host_a -hlu 2 -alu 2
	$NAVICLI storagegroup -addhlu -gname SG_host_b -hlu 3 -alu 3
	$NAVICLI storagegroup -addhlu -gname SG_host_b -hlu 4 -alu 4
	$NAVICLI storagegroup -addhlu -gname SG_host_c -hlu 6 -alu 6
	$NAVICLI storagegroup -addhlu -gname SG_host_c -hlu 7 -alu 7
	$NAVICLI storagegroup -addhlu -gname SG_host_d -hlu 9 -alu 9
	$NAVICLI storagegroup -addhlu -gname SG_host_d -hlu 10 -alu 10
	$NAVICLI storagegroup -addhlu -gname SG_host_e -hlu 12 -alu 12
	$NAVICLI storagegroup -addhlu -gname SG_host_e -hlu 13 -alu 13
	$NAVICLI storagegroup -addhlu -gname SG_host_f -hlu 15 -alu 15
	$NAVICLI storagegroup -addhlu -gname SG_host_f -hlu 16 -alu 16 

	[root@dumbledore greenfield]# more config_hosts 
	#!/bin/ksh

	NAVICLI="/opt/Navisphere/bin/navicli -h 172.17.0.5"

	$NAVICLI storagegroup -sethost -host host_a -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_b -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_c -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_d -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_e -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_f -failovermode 2 -arraycommpath 1

	### /usr/sbin/lpcf/lputil - persistent mappings
	### format - label disks
	### vxinstall
	### vxdiskadm add disks

#### config_hosts

	#!/bin/ksh

	NAVICLI="/opt/Navisphere/bin/navicli -h 172.17.0.5"

	$NAVICLI storagegroup -sethost -host host_a -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_b -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_c -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_d -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_e -failovermode 2 -arraycommpath 1
	$NAVICLI storagegroup -sethost -host host_f -failovermode 2 -arraycommpath 1

	### /usr/sbin/lpcf/lputil - persistent mappings
	### format - label disks
	### vxinstall
	### vxdiskadm add disks

