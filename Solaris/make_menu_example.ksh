#!/bin/ksh
#
# Part of the Solaris build stuff so won't run standalone, but here as a useful
# example of how to use the make_menu function

	releases=$(ls)
	for r in $releases ; do
		[[ -f "$r/description" ]] && desc="$(cat $r/description)" || desc=""
		[[ -f "$r/current_version" ]] && vers="$(cat $r/current_version)" || vers=""
		line=$(printf "%-20s %-10s %s\n" "$r" "$vers" "$desc")
		if [[ -z "$lines" ]] ; then
			lines="$line"
		else
			lines="$lines":"$line"
		fi
	done


	echo ""
	IFS=":"
	make_menu "This script may be invoked as $0 <release>\nno release name was supplied, using interactive mode":$lines
	lineno=$?
	[[ $lineno -eq 0 ]] && exit

	set $lines
	eval line=\${$lineno}

	IFS=" "

	### The line has the version/description - remove these
	set $line; release=$1

