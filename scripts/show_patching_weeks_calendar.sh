#!/bin/bash
#
# show_patching_weeks_calendar.sh
#
# Ade 02/2025
#
# Shows this month and RANGE either side, with the patching week numbers
#
# By default show two months either side, or can modify with -m
# e.g. -m 6 shows 6 months either side of today 
#


### Default number of months either side
RANGE=2

while getopts "m:" opt; do
        case $opt in
                m) RANGE=$OPTARG ;;
                :) echo "Usage: $0 [-m range]" ;;
        esac
done


startmonth=$(( $(date +%m) - $RANGE ))  ; startyear=$(date +%Y)

### In case the start spans the year-end
while [[ $startmonth -lt 1 ]] ; do startmonth=$(( $startmonth + 12 )) ; startyear=$(( $startyear - 1 )) ; done


### Iterate over the months range

for m in $(seq $startmonth $(( $startmonth + $(( $RANGE * 2 )) )) ) ; do

        echo

        ### Deal with multiple years if needed
        displaym=$(( $m % 12 )) ; displayy=$(( $startyear + $(( $m / 12 )) ))

        ### Adjust if rolled over to December
        if [[ $displaym -eq 0 ]] ; then displaym=12 ; displayy=$(( $displayy - 1 )) ; fi

        lineno=-2  ### 'cal' output includes two header lines, so start at -2

        ### If the first of the month is Sat/Sun, delay start by a week
        firstofmonthday=$(date -d "$displayy-$displaym-01" +%u)
        [[ $firstofmonthday -ge 6 ]] && lineno=-3


        ### And display the calendar for the month, with patching weeks added
        IFS=''
        cal -m $displaym $displayy | while read line ; do

                week=$lineno

                out=""
                case $week in 
                        1) out="- Week 1 (Dev & Test)" ;;
                        2) out="- Week 2 (Pre-prod)" ;;
                        3) out="- Week 3 (Production)" ;;
                esac

                printf "%-30s %s\n" "$line" "$out"

                lineno=$(( $lineno + 1 ))
        done

done
