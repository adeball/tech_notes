#!/bin/bash

cat << EOF
###
### /etc/fstab - $(date +%F)
###

EOF

awk '{printf "%-45s %-10s %-7s %-15s %s\n",$1,$2,$3,$4,$5,$6}' /etc/fstab | \
sed -e '/^#/d' | \
sort -u -k 3,3 -k 2,2



