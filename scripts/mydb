#!/bin/ksh
#
# $Id: mydb,v 1.1 2010/01/23 16:08:54 ade Exp ade $
#
# $Log: mydb,v $
# Revision 1.1  2010/01/23 16:08:54  ade
# Initial revision
#
#

[[ -z "$MYDB_HOME" ]] && DB=~/.mydb || DB=$MYDB_HOME
[[ -z "$MYDB_TABLE" ]] && MYDB_TABLE=default
DB="$DB/$MYDB_TABLE"

### Crash/burn!
cb()
{
	printf "Error: %s\n" "$@"
	exit 1
}

### Check/create DB directory
if [[ ! -d "$DB" ]] ; then
	mkdir -p "$DB/data"
	[[ $? -ne 0 ]] && cb "Cannot create/access $DB/data"
	if [[ "$MYDB_TABLE" == "default" ]] ; then
		print "Default table" > $DB/descr
	else
		read d?"Please enter a description for table \"$MYDB_TABLE\": "
		print "$d" > $DB/descr
	fi
fi

cd "$DB/data" || cb "Cannot change directory to $DB/data"


mydb_get_indexes()
{
	ls -1
}


mydb_table()
{
	if [[ "$1" == "descr" ]] ; then
		if [[ $# -eq 2 ]] ; then
			print "$2" > ../descr
		else
			cb "Usage: mydb table descr Description"
		fi
	elif [[ $# -eq 0 ]] ; then
		cd ../..
		ls | while read t ; do
			printf "%-30s: " "$t"
			[[ -f "$t/descr" ]] && cat "$t/descr"
		done
	fi
}

mydb_get()
{
	if [[ $# -eq 0 ]] ; then
		### No args, so return a line of headers line for each index, prepended by "index"
		(printf "index "; mydb_get_fields | while read i ; do printf "%s " "$i" ; done ; print) | sed 's/ $//'
		for index in $(mydb_get_indexes) ; do
			printf "%s " "$index"
			mydb_get "$index"
		done
	elif [[ $# -eq 1 ]] ; then
		(for f in $(mydb_get_fields) ; do
			v="$(mydb_get "$1" "$f")"  
			printf "%s " "$v"
		done
		print ) | sed 's/ $//'
	elif [[ $# -eq 2 ]] ; then
		target="$1/$2"
		[[ -f "$target" ]] && cat $target
		if [[ "$1" == "*" ]] ; then
			mydb_get_indexes | while read i ; do 
				v="$(mydb_get "$i" "$2")"
				[[ -n "$v" ]] && printf "%s %s\n" "$i" "$v"
			done
		elif [[ "$1" == "**" ]] ; then
			mydb_get_indexes | while read i ; do 
				printf "%s %s\n" "$i" "$(mydb_get "$i" "$2")" 
			done
		fi
	fi
}	


### arg=get_fields

mydb_get_fields()
{
	if [[ $# -eq 0 ]] ; then
		### Return a list of all fields
		ls */* | awk -F/ '{print $NF}' | sort -u 
	elif [[ $# -eq 1 && -n "$1" ]] ; then
		if [[ -d "$1" ]] ; then
			cd "$1" 
			ls | sort 
		fi		
	elif [[ $# -eq 1 ]] ; then
		mydb_get_indexes | while read i ; do printf "%s %s " "$i" $(mydb_get_fields "$i") ; done
	fi
}

mydb_put()
{
	[[ $# -ne 3 ]] && cb "usage $0 put index field value"
	mkdir -p "$1" || cb "Error creating $DB/$1"
	v="$3"
	if [[ $v != ${v##* } || $v != ${v##*\n} ]] ; then
		 v="\"$v\""	# Quote if newlines or spaces
	fi
	print "$v" > "$1/$2" || cb "Error writing $DB/$1/$2"
}


### No argument
mydb_noarg()
{
	cb "No argument given - will think of something later!"
}


### Help
mydb_help()
{
cat << EOF
mydb get	 		# Returns index field field field... (by alpha sorted fields)
mydb get "index" 		# Returns field field field (by alpha sorted fields)

mydb put "index" field value	# Adds "index" to db (if not present) with value for field
				# 	overwrites existing values
mydb get_fields [index]	# Just a list of fields (all) or for "index"
EOF
}


arg="$1" ; [[ $# -ge 1 ]] && shift

case "$arg" in 
	get)
		mydb_get "$@"
		;;
	put)
		mydb_put "$@"
		;;
	get_fields)
		mydb_get_fields "$@"
		;;
	get_indexes)
		mydb_get_indexes "$@"
		;;
	help)
		mydb_help "$@"
		;;
	table)
		mydb_table "$@"
		;;
	"")
		mydb_noarg "$@"
		;;
	*)
		cb "Invalid argument"
		;;
esac
