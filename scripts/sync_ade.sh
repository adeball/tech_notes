#!/bin/bash

cd

files=$(find .[a-zA-Z]* -maxdepth 0 -type f -o -type d | grep -v :)
echo $files

rsync -avxz --delete --exclude=*cache* --exclude=*Cache* --exclude ImapMail \
	$files \
	/shared/setup/users/ade/$(hostname)

cp .ssh/id_rsa /shared/setup/users/ade

