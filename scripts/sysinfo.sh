#!/bin/bash

. ./common.sh

cachedir=~/.sysinfo

csv()
{
    printf "%-$1s," $2
}

csvlast()
{
    printf "%-$1s\n" $2
}

[[ $# -ne 1 ]] && fail "Usage $0 hostname"
host=$1

if [[ -f $cachedir/$host ]] ; then
	cat $cachedir/$host
	exit
fi

[[ ! -d $cachedir ]] && mkdir $cachedir

exec > >(tee $cachedir/$host)

vminfo=$(mktemp); ssh -nq $VSPHERE guestinfo.sh $host 2>/dev/null > $vminfo

### Is it a VM
grep -q "No Virtual Machine" $vminfo 
[[ $? -eq 0 ]] && vm="NotVM" || vm="VM"

### VM state
state=$(awk '/guestState/ {print $NF}' $vminfo)

### What is the 'main' (management) IP
ip=$(awk '/ipAddress/ && NF==3 {print $NF}' $vminfo)

### Other IP(s)
ip2=$(awk '/ipAddress/ && NF==6  && ! /net\[0/ {printf "%s ", $NF} END {print ""} ' $vminfo)




### Last satellite check-in

### Centrify

### IPA

### If physical, can we see the drac

### Hostname
hostname=$(awk '/hostName/ {print $NF}' $vminfo)
[[ -z "$hostname" ]] && hostname="unknown"

### Which satellite server
sat="NoSAT"
for h in $hostname ; do
	[[ $(existsonsat $h $NEWSAT) == 1 ]] && sat="SAT5" 
done

### DNS entry
nslookup $hostname > /dev/null
[[ $? -eq 0 ]] && dns="DNS" || dns="NoDNS"

### SSH access (root?)

ssh="NoSSH"

for h in $host $hostname $ip $ip2 ; do
	if [[ "$ssh" == "NoSSH" ]] ; then
		id=$(ssh -nq -o passwordAuthentication=no -o connectTimeout=5 $h "id -u")
		if [[ $? -eq 0 ]] ; then 
			[[ $id -eq 0 ]] && ssh="RootSSH" || ssh="SSH"
		fi
	fi
done


csv     25 $host
csv      5 $vm
csv      7 $state
csv      5 $dns
csv      5 $sat
csv      5 $ssh
csv     15 $ip
csv     15 $ip2 
csvlast 30 $hostname

rm $vminfo
