#!/bin/bash

ROUTER=192.168.1.254
USERPASS=admin:yacorba
CHECKADDR=8.8.123.9
CHECKPORT=53
TIMEOUT=15
WAIT=300

nc -w $TIMEOUT $CHECKADDR $CHECKPORT 
status=$? ; echo status=$status

if [[ $? -ne 0 ]] ; then
	echo "Check connection to $CHECKADDR, port $CHECKPORT failed"
	### Here we need to check a backup host, otherwise we'll be restarting repeatedly
	echo "Rebooting router"
	curl -v --user $USERPASS http://$ROUTER/userRpm/SysRebootRpm.htm?Reboot=Reboot 
	echo "Waiting for $WAIT seconds, then will try resyncing DNS"
	sleep $WAIT
	/root/bin/duck.sh
fi
