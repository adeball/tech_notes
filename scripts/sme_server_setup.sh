#!/bin/bash
#
# Some things to be done after installing a new SME 9.1 server
#

### Update to latest versions

yum --enablerepo=smecontribs install smeserver-updates


### Extra packages

yum -y --enablerepo=base --nogpgcheck install pdksh yum-utils nmap nc git rcs


### NFS

yum install --enablerepo=smecontribs --enablerepo=base smeserver-nfs nfs4-acl-tools

config setprop nfs-rules RULE1 "/home/e-smith/files 192.168.1.0/24(rw,sync,secure_locks,no_root_squash,insecure)"
config setprop nfs-rules RULE2 "/setup 192.168.1.0/24(rw,sync,secure_locks,no_root_squash,insecure)"

signal-event nfs-update
config set UnsavedChanges no

### /setup
echo ; echo "Now copy the /setup directory from the old server" ; echo

cd /usr/lib
ln -s ../lib64/yp
