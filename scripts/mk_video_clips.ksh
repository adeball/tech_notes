#!/bin/ksh

INDIR=/shared/video/Films
OUTDIR=/export/video/pvr/test
mkdir -p $OUTDIR

OFFSET=180
LENGTH=5

> $OUTDIR/Index
i=1
ls $INDIR | sort -R > /tmp/abcd
while read vid ; do
	echo $i "$INDIR/$vid" >> $OUTDIR/Index
	ffmpeg -y -i "$INDIR/$vid" -ss $OFFSET -t $LENGTH -vcodec copy -acodec copy $OUTDIR/film$i.avi
	let i+=1
done < /tmp/abcd
