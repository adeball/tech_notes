#!/bin/bash 


read -s -p "Enter your SSH keyphrase: " PASS
tf=$(mktemp)
chmod 700 $tf
echo "echo $PASS" > $tf
export SSH_ASKPASS="$tf"
#export SSH_ASKPASS=/usr/lib64/seahorse/seahorse-ssh-askpass

### Annoying, but having too many keys means you get auth failures for hosts which are
### not explicitly configured in the config file.  Limit the number...
EXCLUDE="archive|AWS|monsoon|hp5|gitolite|disco|exvpuppet|deployit|id_api"


ssh-add -D

cd $HOME/.ssh
(find . -type f | xargs file | awk -F: '/private/ {print $1}' ; find . -name id_rsa) \
	| egrep -v "$EXCLUDE" \
	| sort \
	| while read privkey 
	do
		echo "Adding $privkey to ssh-agent"
		ssh-add $privkey 
	done

#stty -echo
#ssh exxvmssh01 ssh-add
#ssh exprhnsat6 ssh-add
#ssh mgzdisssh01 "ssh-add ~/.ssh/id_l"
#stty echo

echo
ssh-add -l
echo

rm $tf
/usr/bin/read -s -p "Press Enter to exit: "
echo

