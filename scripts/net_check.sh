#!/bin/bash
#
# Checks hosts for ping & ssh connectivity, and optionally TCP connections in
# both directions.
#
# Variables
# ---------
#
# TIMEOUT: timeout in seconds for ping/ssh/netcat connections



TIMEOUT=5
forwardports="1529 3872"
forwardports=""
reverseports="3872 4903"
reverseports=""
hosts="$@"
#FMT="%-40s %-4s %-4s %-8s"
FMT="%-40s %-4s %-8s"

check_ping()
{
	echo "ign" ; return
        ping -w $TIMEOUT -c 1 $h  > /dev/null 2>&1
        [[ $? -eq 0 ]] && echo "ok" || echo "fail"
}

### Note this is a full ssh check, so takes any hops & credentials in .ssh/config into account 
check_ssh()
{
        host=$1
        uptime=$(ssh -n -q -o connectTimeout=$TIMEOUT $h uptime 2>/dev/null)
        u="$(echo $uptime |  sed 's/.*up //;s/,.*//' | fmt -1000)"
        [[  -n "$u" ]] && echo "ok   $u" || echo "fail"

}

check_port()
{
        host=$1
        port=$2
        nc -w $TIMEOUT $host $port < /dev/null  > /dev/null 2>&1
        [[ $? -eq 0 ]] && echo "ok" || echo "fail"
}

rev_check_port()
{
        host=$1
        port=$2
        status=$(ssh -qn $host "nc -w $TIMEOUT $(hostname) $port < /dev/null  > /dev/null 2>&1; echo \$?")
        [[ $status == "0" ]] && echo "ok" || echo "fail"
}



for p in $forwardports ; do FMT="$FMT %-6s "; done 
for p in $reverseports ; do FMT="$FMT %-7s "; done 

FMT="$FMT \n"

headers="Hostname ssh uptime"
for port in $forwardports ; do headers="$headers to$port " ; done
for port in $reverseports ; do headers="$headers rev$port " ; done
printf "\n$FMT\n" $headers

for h in $hosts ; do

        portstat=""
#        pingstat=$(check_ping $h)
        sshstat="$(check_ssh $h)"
        for p in $forwardports ; do
                portstat="$portstat $(check_port $h $p)"
        done
        for p in $reverseports ; do
                portstat="$portstat $(rev_check_port $h $p)"
        done
        printf "$FMT" $h "$sshstat" $portstat

done 

echo

