#!/bin/bash

# Touch this script, otherwise it gets deleted after a few days
touch $0

### Multiply by 100, to convert to scaled integer (uptime only reports to 
### two decimal places)

oneminavg=$(uptime | awk '{split($(NF-2),a,","); print a[1]*100}')

### If load is over 10 (use 10*100, i.e. 1000 for int comparison), record ps output

if [[ ${oneminavg} -gt 1000 ]] ; then
        cd $(dirname $0)
        exec > ps.$(date +%FT%T)
        uptime 
        echo
        ps -eo user,pid,ppid,pcpu,size,vsize,cputime,%cpu,%mem,args --sort -%mem | \
                head -100 | \
                 awk '{printf "%-10s %-6s %-6s %-4s %-9s %-9s %-10s %-4s %-4s %s\n", 
                        $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}'

        ### If we're in trouble, kill firefox
        if [[ ${oneminavg} -gt 3000 ]] ; then
                echo ; echo "Load average is excessive, killing firefox!"
               echo "Firefox processes found..."
                ps -ef | awk '/firefox/ && !/awk/'
                echo ; echo "Killing them..."
                ps -ef | awk '/firefox/ && !/awk/ {print $2}' | xargs kill
        fi
fi

