#!/bin/bash
#
# -g : port forward
# -N : Don't run a command
# -f : Run in background
# -L : forward spec (NB: localhost is the private IP of the remote system)
#

ps -ef | awk '/ssh.*-L/ && !/awk/ {print $2}' | xargs kill

#port=22022
#hp5ip=$(getent hosts hp5.co.uk | awk '{print $1}')
#[[ $hp5ip == "192.168.1.128" ]] && port=22


###### Via AWS box

### Run hp5.co.uk mail services on localhost, ports 1465 & 1993
ssh -g -N -f -L 1465:mail.hp5.co.uk:465 -L 1993:mail.hp5.co.uk:993 zuko
#ssh -g -N -f -L 1587:smtp.travelsmtp.com:587 -L 1993:mail.hp5.co.uk:993 zuko

### ux1.co.uk mail services on localhost, ports 1466 & 1994
ssh -g -N -f -L 1466:mail.ux1.co.uk:465 -L 1994:mail.ux1.co.uk:993 zuko


###### Via Lupin

### Squid 
ssh -g -N -f -L 0.0.0.0:3128:localhost:3128 dumbledore

### VM consoles - RDP
### 3001 -> Dumbledore
###
ssh -g -N -f  -L 0.0.0.0:3001:hagrid:3001 lupin
ssh -g -N -f  -L 0.0.0.0:8080:localhost:80 lupin
ssh -g -N -f  -L 0.0.0.0:3001:localhost:3001 hagrid
