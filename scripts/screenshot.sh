#!/bin/bash
#
# Normally just want to pick a bit of screen and dump to a unique file
# that's what this does...
#

### Check for existing files
lastfile=$(ls [0-9][0-9][0-9].jpg 2>/dev/null | tail -1)

if [[ -z "$lastfile" ]] ; then 
	nextfile=001.jpg
else
	l=$(basename $lastfile .jpg)
	nextfile=$(printf "%3.3d.jpg\n"  $(( $l + 1 )))
fi


echo -n Saving $nextfile, select the area you require...

gnome-screenshot --gapplication-service -a -f $nextfile

echo " done"
