#!/bin/bash
#
# ssh_setup - Ade 30/09/2004
#
# $Id$
#
# Takes the pain out of setting up ssh

cd

if [[ "$1" = "" ]] ; then
	echo
	echo "Where are you wanting to login to?"
	echo "You can specify just a hostname, IP, user@hostname or user@IP"
	echo "If you do not specify a user, your local username $USER will be used"
	echo "e.g. bigbox, bigbox.test.co.uk, 10.0.0.1, user@bigbox, user@bigbox.test.co.uk or user@10.0.0.1"
	echo
	echo -n "Please enter the remote host [and user if needed]: "
	read host
else
	host=$1
fi

hostpart=$(echo $host |awk -F@ '{print $2}')

### If we have getent, check resolution
which getent > /dev/null 2>&1
if [[ $? -eq 0 ]] ; then 
	getent hosts $hostpart > /dev/null
	if [[ $? -ne 0 ]] ; then
		echo
		echo "Cannot resolve $hostpart, please check..."
		echo
		exit 1
	fi
fi

### Let's just check if we're already set up

ssh -o BatchMode=yes $host echo > /dev/null 2>&1
if [[ $? -eq 0 ]] ; then
	echo
	echo "You already have passwordless SSH access to $host set up correctly."
	echo "Doing nothing..."
	echo
	exit
fi

### Check if we have keys generated already

[[ ! -d .ssh ]] && mkdir -p .ssh

if [[ ! -f .ssh/id_rsa ]] ; then
	echo "No local RSA keys - will generate some for you..."
	/usr/local/bin/ssh-keygen -t rsa -P "" -f .ssh/id_rsa
	[[ $? -ne 0 ]] && /usr/bin/ssh-keygen -t rsa -P "" -f .ssh/id_rsa
fi

echo "Making remote .ssh directory. You'll be asked for your remote password..."
ssh $host "mkdir -p .ssh; chmod 700 .ssh"
echo
echo "Copying local public key to $host.  You'll be asked again..."
cat .ssh/id_rsa.pub | ssh $host "cat >> .ssh/authorized_keys && chmod 600 .ssh/authorized_keys"

ssh -o BatchMode=yes $host echo 
if [[ $? -eq 0 ]] ; then 
	echo "Success! Passwordless SSH access is now enabled to $host"
	echo
else
	echo
	echo "Something has gone awry..."
	echo
	exit 1
fi
