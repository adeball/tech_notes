#!/bin/bash
#
# -g : port forward
# -N : Don't run a command
# -f : Run in background
# -L : forward spec (NB: localhost is the private IP of the remote system)
#


ssh -g -N -f \
	-L 8000:satellite01:443  \
	-L 8001:satellite02:443  \
	-L 8002:ddi:443          \
	-L 8003:fbs-lin-50188:39000 \
	-L 8004:fbs-lin-29901:39000 \
uolra

