#!/usr/bin/wish
#
# Title: Xmenu
# 
# Author: Adrian Ball
#
# Copyright: GPL
#
# Last revision: 04/02/1999
#

set BASE /users/ade/xmenu
set LIB $BASE/lib
set BIN $BASE/bin
set MENUDIR $LIB/menus
set LOCAL /users/ade/.xmenu
set SESSION_MANAGER 0


############################################################
#
# getargs
# does what it says

proc getargs argv {

    global SESSION_MANAGER
    set valid_args {"-session"}
    
    foreach arg $argv {
	if {[lsearch $valid_args $arg] != -1} {
	    switch -- "$arg" {
		-session
		{
		    puts "Running in session manager mode"
		    set SESSION_MANAGER 1
		}
	    }
	}
    }
}

############################################################
#
# runcmd
# Runs commands
# cmd :== {item1 item2}
# item1 :== text (description of command)
# item2 :== text (executable command)

proc runcmd cmd {
    puts "starting [lindex $cmd 0] ([lindex $cmd 1])..."
    eval exec [lindex $cmd 1] &
}


############################################################
#
# runwm
# Runs Window manager

proc runwm {} {

    global LOCAL

    if {[file exists $LOCAL/window_manager] !=0 } {
	runcmd [list "Default window manager" [read_file $LOCAL/window_manager]]
    } else {
	write_file $LOCAL/window_manager fvwm2
	runwm
    }
}


############################################################
#
# runothers
# Runs additional executables after Xmenu has started

proc runothers {} {

    global LOCAL

    if {[file exists $LOCAL/extra_clients] != 0 } {
	exec $LOCAL/extra_clients &
    } else {
	write_file $LOCAL/extra_clients \
		"# Put apps to start automatically after Xmenu here"
	chmod 0755 $LOCAL/extra_clients
    }
}	


############################################################
#
# build_menu_bar
# A bit flaky at the moment, creates menus based on the 
# files in LIB

proc build_menu_bar {} {

    global MENUDIR

    destroy .xmenu
    menu .xmenu -tearoff 0

    .xmenu add cascade -label "File" -menu .xmenu.file -underline 0

    menu .xmenu.file

    .xmenu.file add command -label "Recreate menus" -command build_menu_bar
    .xmenu.file add command -label "Exit" -command {destroy .}

    # Read menu source files
    set i 0
    foreach mf [readdir $MENUDIR] {
	if {[regexp {~$} $mf] == 0} {
	    set menuid .xmenu.$i
	    incr i
	    for_file line $MENUDIR/$mf {
		set bits [split $line :]
		if {[lindex $bits 0]=="title"} {
		    set title [lindex $bits 1]
		    menu $menuid -tearoff 0
		    .xmenu add cascade -label "$title" -menu $menuid -underline 0
		}
		if {[lindex $bits 0]=="item"} {
		    $menuid add command -label "[lindex $bits 1]" \
			    -command "runcmd \"[lrange $bits 1 2]\""
		}
	    }
	}
    }

    . configure -menu .xmenu -width 420

}

getargs $argv
build_menu_bar


# If we are running in session manager mode, start a window manager
# and run anything in $HOME/.xmenu/extra_clients

if {$SESSION_MANAGER == 1} {
    runwm
    runothers
}






