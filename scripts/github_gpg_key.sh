#!/bin/bash

name="Github AdrianBallMO"
email=adrian.ball@metoffice.gov.uk

gpg2 --batch --gen-key <<EOF
Key-Type:1
Key-Length:2048
Subkey-Type:1
Subkey-Length:2048
Name-Real: $name
Name-Email: $email
Expire-Date: 0
EOF


signingkey=$(gpg --list-secret-keys --keyid-format=long "$name" | sed -n '/sec/s-.*/--p' | sed 's/ .*//')

echo "Add signingkey = $signingkey to the $name section in ~/.gitconfig"

echo
echo "Then add this as a GPG key on github (under settings)"
echo

gpg --armor --export "$name"  

