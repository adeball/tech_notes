###Basic commands

* adinfo: shows the domain a system is joined to, plus other useful info
* adquery user: Lists all users (passwd format)
* adquery user <account>: passwd line for that user

* Use adflush if changes are made, otherwise they won't be picked up!


###Switch domain

* When you initially launch `DirectManage Access Manager` you may not see any objects - Right-click on 'DirectManage Access Manager' and select 'options...', then the filter settings tab,  ensure all the options are ticked (Load all Zones, Show disabled Active Directory accounts, Show orphans,Show Auto Zone)
* To switch between managing domain1 and domain2 domain objects, right click on 'DirectAccess Access Manager' under 'Console Root' and click 'Connect to Remote Forest'. When asked for a domain controller simply enter 'domain1' or 'domain2' to switch domains.
* No objects in domain: Right-click on domain, options, filter settings, load all zones + show orphans

###Add user e.g. Martin Osborne to cmpd1.metoffice.gov.uk/Centrify/Zones/general-science-dev 

#### In ADU&C
* Create AD account: Use ADUC
* Go to cmpd1/Centrify/Users/Development 
* Add user here e.g. display name = "Osborne, Martin (Development)"
* Set a password, with 'user must change at next login'

#### In centrify access manager
* Go to Centrify/Zones/general-science-dev/UNIX Data/Users
* Right-click and add user to zone
* Select a shorter UID if necessary (check getent passwd on a host in the zone for a unique number), home is /home/mosborne, shell is /bin/bash

* Go back to ADUC and add the user to the correct group(s) (Using the 'memberof' tab)
* The user should be visible with 'getent passwd mosborne' on a host in the zone after a 'service centrifydc restart'

NB: Apparently there is a 'create like' option somewhere, which would set up the groups etc the same as an existing account

