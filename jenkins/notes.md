# Reset password
Edit config.xml
(On MacOS: it's in ~/Jenkins/Home )

Set UserSecurity to false (only do this on a local test server)

Restart

MacOS: 
launchctl unload /Library/LaunchDaemons/org.jenkins-ci.plist
launchctl load /Library/LaunchDaemons/org.jenkins-ci.plist
(Just normal service/init.d stuff on Linux)
