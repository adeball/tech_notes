
### Useful Git log alias
alias gl='git log --oneline --abbrev-commit --all --graph --decorate --color'

### References

* http://think-like-a-git.net/sections/testing-out-merges/the-savepoint-pattern.html

### Github remote 
* Show: git remote -v
* Set push (e.g.): git remote set-url --push origin ssh://git@github.com/adeball/tech_notes.git (NB: Always use git@github.com, not the normal username.  Copy id_rsa.pub as a deploy key for the repo).
