* MacOS
brew install ansible
#Increase file handles for OS X
sudo launchctl limit maxfiles unlimited

#Create a hosts file
e,g. echo "127.0.0.1" > ansible_hosts
export ANSIBLE_INVENTORY=~/ansible_hosts
