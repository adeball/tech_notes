### Some useful examples of openssl commands

The following commands assume we have /etc/pki/tls/{certs|private} - these seem to be the standard locations.

The file extensions do not need to be .key, .crt or .csr, but these extensions help - the files also do not need to be named by server, but again - it helps!

NB: Files can be anywhere, with any name - so it's always worth checking the application configuration if you think any keys have been set up in the past. E.g. there may be an existing private key hanging around under /etc/httpd. grep -i ssl /etc/httpd/conf.d/* is a good place to start looking. Also, if there is more than one application using SSL, they do not need to use the same keys, but can. It's probably best to keep everything under /etc/pki/tls... and (if separate keys are required) name them by application. This will give anyone trying to unravel the configuration a fighting chance of finding and identifying things.

Very useful/well-written article explaining the SHA-1 problem: https://konklone.com/post/why-google-is-hurrying-the-web-to-kill-sha-1


### Reference

https://www.madboa.com/geek/openssl/



#### Cut/paste/modify to suit

    SERVER=server.my.co.uk
    C=GB
    ST=County
    L=Town
    O="Very Big Co"
    OU="The Awesome Web Team"
    EMAIL=webteam@my.co.uk


###### Generate a key

    cd /etc/pki/tls/private
    openssl genrsa -out $SERVER.key 2048


###### Set up for Subject Alternate Names in a CSR

    # Edit (/etc/pki/tls)/openssl.conf (default name, the current pwd will be checked for this file, you can 
    # also point to a different conf file).  Ensure the sections below contain lines similar to these.

    [req]
    req_extensions = v3_req

    [ v3_req ]

    # Extensions to add to a certificate request

    basicConstraints = CA:FALSE
    keyUsage = nonRepudiation, digitalSignature, keyEncipherment
    subjectAltName = @alt_names

    [alt_names]
    DNS.1 = $SERVER
    DNS.2 = alias1.fqdn
    DNS.3 = etc


###### Generate new key and CSR in one go

    cd /etc/pki/tls/private
    openssl req -out ../certs/$SERVER.csr -new -newkey rsa:2048 -sha256 -nodes -keyout $SERVER.key


###### Generate CSR from existing key

    cd /etc/pki/tls/private
    openssl req -out ../certs/$SERVER.csr -new -sha256 -key $SERVER.key

###### Generate CSR from existing key, with questions pre-answered (NB: this uses an existing key, but you can specify a new one with -newkey rsa:2048)

    openssl req -new -key $SERVER.key -sha256 -nodes -out ../certs/$SERVER.csr \
      -subj "/C=GB/ST=$ST/L=$L/O=$O/OU=$OU/CN=$SERVER/emailAddress=$EMAIL"

###### Self-sign a cert

    cd /etc/pki/tls/certs
    openssl x509 -req -sha256 -days 365 -in $SERVER.csr -signkey ../private/$SERVER.key -out $SERVER.crt


#### Checks

###### Check CSR

    cd /etc/pki/tls/certs
    openssl req -text -noout -verify -in $SERVER.csr


###### Check key

    cd /etc/pki/tls/private
    openssl rsa -in $SERVER.key -check

###### Check cert

    cd /etc/pki/tls/certs
    openssl x509 -in $SERVER.crt -text -noout
    openssl x509 -in $SERVER.crt -noout -subject -enddate # e.g. for end date only

####### CA cert (to see all certs in bundle)
while openssl x509 -noout -text; do :; done < cert-bundle.pem


    Basic remote check: curl -v https://site/ > /dev/null   (header is written to stderr)
    Full remote check: SERVER=hostname ; echo | openssl s_client -showcerts -servername $SERVER -connect $SERVER:443 2>/dev/null | openssl x509 -inform pem -noout -text



###### Check a key CSR cert all go together

    cd /etc/pki/tls/certs
    openssl pkey -in ../private/$SERVER.key -pubout -outform pem | sha256sum
    openssl x509 -in $SERVER.crt -pubkey -noout -outform pem | sha256sum
    openssl req -in $SERVER.csr -pubkey -noout -outform pem | sha256sum 

    The hashes output should match.


###### Check a web site for TLS 1.2

openssl s_client -connect mywebsite.com:443 -tls1_2


###### Check a cert date using curl

curl -Iv https://site/   (might need -Ivk if internally signed)

#### Store private key in password manager
Using whatever method is used at the site...


#### Create a keystore for use by Apache Tomcat
NB: You may need a chain etc, in which case there are other parameters to add - but the following works
as a minimum.

    cd /etc/pki/tls
    openssl pkcs12 -export -in certs/$SERVER.crt -inkey private/$SERVER.key -out /path/to/tomcatkeystore -name tomcat
    Enter a password.


Edit the tomcat server.xml file to point to the keystore, and use the password you just entered.
```
 <!-- Define a SSL HTTP/1.1 Connector on port 8443
         This connector uses the JSSE configuration, when using APR, the
         connector should be using the OpenSSL style configuration
         described in the APR documentation -->
    <Connector port="8443" protocol="HTTP/1.1" SSLEnabled="true"
               maxThreads="150" scheme="https" secure="true"
               keystoreFile="/path/to/tomcatkeystore" keystorePass="the-password-you-just-entered"
               clientAuth="false" sslProtocol="TLS" />
```

### Convert p7b file to PEM format
openssl pkcs7 -print_certs -in certs/$SERVER.p7b -out certs/$SERVER.chain.crt 
or for binary
openssl pkcs7 -print_certs -inform der -in certs/$SERVER.p7b -out certs/$SERVER.chain.crt 

### Encrypt/Decrypt

#### Encrypt a stream on the command line

    openssl enc -aes-256-cbc -salt -in file.txt \
      -out file.enc -pass file:/path/to/secret/password.txt
or

    openssl enc -aes-256-cbc -salt -in file.txt \
      -out file.enc -pass pass:mySillyPassword

#### Decrypt
    openssl enc -d -aes-256-cbc -in backup.tar.gz.enc -out backup.tar.gz


### Convert p12 to pem

    openssl pkcs12 -in path.p12 -out newfile.crt.pem -clcerts -nokeys
    openssl pkcs12 -in path.p12 -out newfile.key.pem -nocerts -nodes



### Chain stuff

openssl verify -CAfile /etc/pki/tls/certs/chain.pem /etc/pki/tls/certs/$SERVER.crt

https://www.ssllabs.com/ssltest/index.html  (check the "don't show on boards" option)

