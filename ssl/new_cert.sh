#!/bin/bash
#
# Cobbled together script to 'do the thing' WRT renewing SSL certs with subject alt names
# NB: This works for Apache, YMMV for other software
# 

echo
echo
echo "Here are (probably) the locations you need to refer to..."
echo
awk '/SSL.*Cert/ && !/^#/' /etc/httpd/conf.d/*.conf 
echo
echo


### Fill in your details here, examples are just that...
C=GB
ST=County	
L=Town		
O="Very Big Co"
OU="The Awesome Web Team"
EMAIL=webteam@my.co.uk


### Make sure that you have /etc/pki/tls/openssl.cnf set up for Subject Alternate Names
### Already.  Hint: Ifi these are not found, this script will output some basic instructions, 
### so you can peek ahead.



################################ Do the thing ########################################################


### Let's get some consistency around here.
### Assume that the server has a correctly set up FQDN, and stick with names based on that, rather than
### 'cert.cer' etc, which are not that helpful.
SERVER=$(hostname -f)

### If the keyfile doesn't exist in this location, link it to the real location, 
### that way the next person can simply run this script to renew.
KEYFILE=private/$SERVER.key


cd /etc/pki/tls
echo "Generating CSR..."

openssl req -new \
	-key $KEYFILE \
	-sha256 -nodes \
	-out $SERVER.csr \
	-subj "/C=$C/ST=$ST/L=$L/O=$O/OU=$OU/CN=$SERVER/emailAddress=$EMAIL"


echo "Checking CSR for Subject alt names"
openssl req -text -noout -verify -in $SERVER.csr | grep -q "X509v3 Subject"

if [[ $? -ne 0 ]] ; then 
	cat <<-EOF

	No subject alt names found, you probably need some stuff in ./openssl.cnf
	Check for the following (you need at least DNS.1 = $SERVER and the 'req' bits)

	[req]
	req_extensions = v3_req

	[ v3_req ]

	# Extensions to add to a certificate request

	basicConstraints = CA:FALSE
	keyUsage = nonRepudiation, digitalSignature, keyEncipherment
	subjectAltName = @alt_names

	[alt_names]
	DNS.1 = $SERVER
	DNS.2 = alias1.fqdn
	DNS.3 = etc

	EOF
else


	cat <<-EOF

	Copy $(hostname):/etc/pki/tls/$SERVER.csr to a Windows system, and follow the instructions
	(probably in Sharepoint-landing-page/Certificates) to generate the
	cert ($SERVER.crt) and chain ($SERVER.p7b).
	[ Probably something like:
                   set SERVER=....
                   Certreq –submit –attrib “CertificateTemplate:Web_Server_v1.0” %SERVER%.csr %SERVER%.crt %SERVER%.p7b 
                   Then pick MO-Issuing-CA2 exxcaissprd2
        ]


	Transfer the .crt and .p7b files back here, and copy them to /etc/pki/tls/certs 
	(NB: This is just for reference, the application may need copies elsewhere)

	Convert the .p7b file to a useable-by-Apache format as follows:
	openssl pkcs7 -print_certs < certs/$SERVER.p7b > certs/$SERVER.chain.crt 

	Copy files into the correct locations, as required

	EOF

fi
