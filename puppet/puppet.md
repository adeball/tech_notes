
### Find what files are managed on a client

puppet_ls 
or
puppet catalog select --terminus rest $(hostname -f) file

### Find packages (e.g. - works for other resources too)
puppet catalog select --terminus rest $(hostname -f) package


