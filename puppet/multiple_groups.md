### Note of the syntax

    group { "user_one":
      ensure => present,
      gid => 500,
    }
    group { "user_two":
      ensure => present,
      gid => 501,
    }
    group { "dev_site_one":
      ensure => present,
      gid => 502,
    }
    user { "user_one":
      ensure => present,
      uid => 500,
      gid => 500,
      groups => ["user_two","dev_site_one"],
    }
