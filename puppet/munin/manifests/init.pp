# == Class: munin
#
# This class deploys the munin client. 
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
# # Using the default exxxlinman01 & exvmuninprd01
# include munin
# or
# class { munin: }
#
# # Or for a different server (e.g.)
# class { munin: $munin_servers=> ['mgzmuninprd01'], 
#                $munin_server_ips => ['10.184.101.24'], 
#                $munin_type => 'client|server' }
#
# === Author
#
# Ade Ball <ade@ux1.co.uk>
#
#

class munin ($munin_servers=['exxlinman01','exvmuninprd01'], 
              $munin_server_ips=['151.170.127.94', '10.153.71.46'], 
              $munin_type='client') {
                
  include yum::repos::rh6_epel_x86_64 
  
  package { ['munin-common','munin-node']:
    ensure  => present,
    require => Class['yum::repos::rh6_epel_x86_64'],
  }
  
  if $munin_type == 'server' {
    package { ['munin','munin-cgi','munin-async','rsync']:
    ensure  => present,
    }
    include apache
    user { "apache":
        groups => ['munin'],
    }
    
    file { "/etc/httpd/conf.d/munin-cgi.conf": ensure => "absent" }

    file { "/etc/httpd/conf.d/munin.conf":
      path  =>  "/etc/httpd/conf.d/munin.conf",
      owner => root,
      group => root,
      mode  => 644,
      content => template("munin/munin.conf.erb"),
      notify  => Service[httpd],
    }
    
    file { "/var/www/html/index.html":
      path => "/var/www/html/index.html",
      content => template("munin/index.html.erb"),
      owner   => root,
      group   => root,
      mode    => 644,
    }
    
  }
  
  service { 'munin-node':
    ensure     => running,
    enable     => true,
    name       => 'munin-node',
    hasstatus  => true,
    hasrestart => true,
  }
  
  file { "/etc/munin/munin-node.conf":
    path  =>  "/etc/munin/munin-node.conf",
    owner =>  root,
    group =>  root,
    mode  =>  644,
    content => template("munin/munin-node.conf.erb"),
    notify  => Service[munin-node], 
    require  => Package['munin-node'],
  }
}
