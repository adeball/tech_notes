
To set up a user password in puppet, don't copy the shadow entry, instead use: openssl passwd -1

# e,g.

lucius ade ~ $ openssl passwd -1
Password: 
Verifying - Password: 
$1$q.gyAOpT$9wXzHVKhiEXy.qArfJ6in/

# Then:

user {'fred': password => 'i$1$q.gyAOpT$9wXzHVKhiEXy.qArfJ6in/', }
