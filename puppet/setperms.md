### setperms

Shortcut to setting permissions/ownership on multiple files in the same location.
Just pass the array of names, and set path - obviously can be extended.


    define setperms (
      $path='/path/to/all/files', 
      $owner=username, 
      $group=groupname, 
      $mode,
      ) 
      {
        $paths = regsubst($title, '^', "$path/" )
        file {$paths :
          ensure => 'present',
          owner  => $owner,
          group  => $group,
          mode   => $mode,
      }



e.g. 

    setperms { [ 'file1', 'file2', 'file3' ]: mode => 2750 }
    setperms { [ 'file4', 'file5' ]: mode => 0600 }


NB: You can also use File {...  (note the uppercase F) to set defaults - but use with care as
it may affect file declarations elsewhere.
