#
# Ade 2016-01
#
# Makes use of the standard Puppet Apache module
# NB: set $env in the node manifest, should be production, uat etc
#

class myapachesite {  

  ### The puppetlabs apache module does all we need to set up a redirect, load modules etc

  class { '::apache': mpm_module => 'prefork', }
  include '::apache::mod::php'
  include '::apache::mod::rewrite'


  ### Declare all sites together
  ### e.g. myapachesite-uat, with directory /var/www/html/myapachesite
  ### $nameext is defined in the node manifest
  ### Horrible, and hacky in this version - see: https://docs.puppetlabs.com/puppet/latest/reference/lang_iteration.html

  define puppet::website::setupsite ($website = $title) {
    apache::vhost { [   "${website}${nameext}", 
			"${website}${nameext}.my.domain.net", 
			"${website}.com", 
			"${website}.myexternaldomain.com", 
			] :
	serveraliases => ["www.${website}.com",],
        port    => '80',
        docroot => "/var/www/html/${website}",
        directories => [
  	  { 
            path => "/var/www/html/${website}",
            allow_override => 'All',
          }
        ],
    }
    
    file { "/var/www/html/${website}":
	ensure => directory,
	owner  => root,
        group  => root,
	mode   => 755,
    }

  }

  $websites = [ 'website1', 'website2', ]
  puppet::website::setupsite { $websites: }


 
  # Create mountpoints
  file { [ "/var/www/html", "/scratchspace", ]:
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => 755,
  }

  file { [ "/var/lib/mysql", ]:
    ensure => directory,
    owner  => mysql,
    group  => mysql,
    mode   => 755,
  }


  ### Create swap file - not elegant, but it works...
  ### Puppet LVM is a bit flaky to get this working using volumes
  exec { "Create swap file":
    command => "/bin/dd if=/dev/zero of=/swapfile bs=1M count=4096",
    creates => "/swapfile",
  }
 
  exec { "Attach swap file":
    command => "/sbin/mkswap /swapfile && /sbin/swapon /swapfile",
    require => Exec["Create swap file"],
    unless => "/sbin/swapon -s | grep /swapfile",
  }
   

  # Create volumes
  class { 'lvm':
    volume_groups    => {
      'data' => {
        physical_volumes => [ '/dev/xvdb', ],
        logical_volumes  => { 
          'html'      => { 'size' => '5G', 'mountpath' => '/var/www/html',  'mountpath_require' => true, }, 
          'scratchspace' => { 'size' => '3G', 'mountpath' => '/scratchspace',  'mountpath_require' => true, }, 
          'mysql'     => { 'size' => '5G', 'mountpath' => '/var/lib/mysql', 'mountpath_require' => true, }, 
        },
      },
    },
  } ->

  # Install Apache/MySQL/PHP (MySQL server needs to follow the volume creation)
  package {php5: ensure => latest }
  package {php5-mysql: ensure => latest }

  # Avoid deadlock, cannot create LVM volume before mysql-server installed, but cannot create volume without mysql account 
  group { 'mysql': ensure => 'present', gid => '119', } ->
  user  { 'mysql': ensure => 'present', uid => '111', gid => '119', home => '/nonexistent', shell => '/bin/false', password => '!!', } ->
  package {mysql-server: ensure => latest } ->
  package {mysql-client: ensure => latest }

  group {'postfix': ensure => 'present', gid => '117', } ->
  group {'postdrop': ensure => 'present', gid => '118', } ->
  user  { 'postfix': ensure => 'present', uid => '110', gid => '117', home => '/var/spool/postfix', shell => '/bin/false', password => '!!', } ->
  package {mailutils: ensure => latest }

  file {'/etc/postfix/main.cf':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    content => template("routledgecompanion/etc/postfix/main.cf.erb"),
  }

  firewall { '010 allow ports for Apache':
    dport   => ['80', '25', ],
    proto  => 'tcp',
    action => 'accept',
  }

  ### Add dev keys for non-production systems.  No idea why two if statements are needed. Puppet is being a PITA.
  ### When set up is finished, uncomment the lines which restrict this to prod
  #if $::hostname !~ /prod/ { file { '/home/appuser/.ssh' : ensure => 'directory', owner  => 'appuser', group  => 'apps', mode   => '644', } }
  #if $::hostname !~ /prod/ { include 'dev-keys' }

  file { '/home/appuser/.ssh' : ensure => 'directory', owner  => 'appuser', group  => 'apps', mode   => '644', }
  include 'dev-keys'

  ### And allow full sudo on the non prod servers
  if $::hostname !~ /dmzi/ { file { '/etc/sudoers.d/dev' : ensure => 'present', mode => '0440', content => 'appuser ALL=(ALL) NOPASSWD:ALL' }}

  include aws-db-backups
  cron { 'Backup-DB':
      command => '/usr/local/bin/generic-aws-db-backup.sh',
      require => File['AWS-Backup-script'],
      user    => root,
      minute  => 35,
      hour    => 02,
      ensure  => present,
  }


}
