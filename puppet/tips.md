
* Show the top level manifest: puppet config print manifest --section master
* List resource types: puppet resource --types
* Show a resource, e.g.: puppet resource file /etc/hosts
* List info about the current node: facter

