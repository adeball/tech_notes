
Use RH/OracleLinux/CentOS 5 (used Oracle 5.8).

For OVM Manager use runInstaller.sh -n (no deps,as they are fine anyway)
Set ulimit -n 8192 in /etc/bashrc
Add oracle user, and dba group
OVM Manager needs 8GB min

Server discovery to add OVM server(s)
Need NFS or SAN storage (used FreeNAS) - make sure the storage is "refreshed"
Create server pool

Create a repository, using the NFS mount specified.
Create a network (cannot be the the mgmt network)

On the OVM server, use the "xm" command to list/do stuff with the domains
