### Some useful examples of openssl commands

The following commands assume we have /etc/pki/tls/{certs|private} - these seem to be the standard locations.

The file extensions do not need to be .key, .crt or .csr, but these extensions help - the files also do not need to be named by server, but again - it helps!

NB: Files can be anywhere, with any name - so it's always worth checking the application configuration if you think any keys have been set up in the past. E.g. there may be an existing private key hanging around under /etc/httpd. grep -i ssl /etc/httpd/conf.d/* is a good place to start looking. Also, if there is more than one application using SSL, they do not need to use the same keys, but can. It's probably best to keep everything under /etc/pki/tls... and (if separate keys are required) name them by application. This will give anyone trying to unravel the configuration a fighting chance of finding and identifying things. 

Very useful/well-written article explaining the SHA-1 problem: https://konklone.com/post/why-google-is-hurrying-the-web-to-kill-sha-1 

###### Cut/paste 

    SERVER=myserver.fqdn.co.uk

###### Generate a key

    cd /etc/pki/tls/private
    openssl genrsa -out $SERVER.key 2048

###### Generate new key and CSR in one go

    cd /etc/pki/tls/private
    openssl req -out ../certs/$SERVER.csr -new -newkey rsa:2048 -sha256 -nodes -keyout $SERVER.key

###### Generate CSR from existing key

    cd /etc/pki/tls/private
    openssl req -out ../certs/$SERVER.csr -new -sha256 -key $SERVER.key 


###### Check CSR

    cd /etc/pki/tls/certs
    openssl req -text -noout -verify -in $SERVER.csr 


###### Check key

    cd /etc/pki/tls/private
    openssl rsa -in $SERVER.key -check

###### Check cert

    cd /etc/pki/tls/certs
    openssl x509 -in $SERVER.crt -text -noout

###### Self-sign a cert

    cd /etc/pki/tls/certs
    openssl x509 -req -sha256 -days 365 -in $SERVER.csr -signkey $SERVER.key -out $SERVER.crt

###### Check a key belongs to a cert
https://kb.wisc.edu/middleware/page.php?id=4064

    cd /etc/pki/tls/certs
    openssl x509 -noout -text -in $SERVER.crt
    openssl rsa -noout -text -in ../private/$SERVER.key

Compare the modulus and public exponent sections, they should match

### Store private key in password manager
However this is done at the site...
