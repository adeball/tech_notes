
# Veritas Cluster Server (VCS) notes

[6.0.1 PDF doc] 
(https://download.veritas.com/resources/content/live/DOCUMENTATION/5000/DOC5521/en_US/vcs_admin_60pr1_sol.pdf?__gda__=1459928561_e426ce21f96f41838f27531bf19e042e)

* PATH=$PATH:/opt/VRTS/bin
* Show status: hastatus -summary
* GUI: hagui (NB: Java based, may need downloading for more recent versions - or use the Veritas Operations Manager client)
* Main config files are in /etc/VRTSvcs/conf/config - main.cf (& types.cf)

## Terms
* *LLT*: Low Latency Transport (NB: replaces IP transport in VCS comms)
* *GAB*: Group Membership Services & Atomic Broadcast - monitors cluster membership and provides reliable cluster comms.
* *AMF*: Asynchronous Monitoring Framework - kernel driver for event notifications (e.g. fs unmounted)
* *IMF*: Intelligent Monitoring Framework 

## Processes
* had: High Availability Daemon (AKA VCS engine)

## Logs
/var/VRSTvcs/log
