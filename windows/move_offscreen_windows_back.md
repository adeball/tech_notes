
E.g. when multi-monitor config has changed.

One site says that cascading windows works (right-click taskbar) - no luck for me.

This works (Win 7)
* Use alt-tab to select the window
* Use shift-win-leftarrow (or right/up/down) 
* Some of the window should now be visible, grab it and resize a bit - the titlebar should now be accessible

