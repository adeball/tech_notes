
e.g. to change extensionAttribute13 for a group (used here for group name mapping in Linux)

1. Start ADUC
2. Go to view, and select 'advanced features'
3. Search for the group you're looking for
4. Open the properties dialogue and go to the 'object' tab
5. Note the path to the object under 'canonical name of object'
6. Now go back to the main ADUC window and use the navigator panel to find the object again, by going down the folders
7. Open properties (again!), this time it should have the attribute editor tab
8. Change the value as required
