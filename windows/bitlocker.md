
When encrypting, bitlocker reserves all but about 6GB of the drive.
As this is usually running at the time you're installing/retrieving backups, it's a pain...

You can regain the full 6GB (so you may need to run more than once) by pausing and resuming the encryption.

i.e.
```
manage-bde.exe -pause c:
manage-bde.exe -resume c:
```
and `manage-bde.exe status` will show where it's at.

### Get recovery key
manage-bde -protectors c: -get

### Disable temporarily (e.g. for BIOS update etc), then enable to restart
manage-bde -protectors -disable C:
