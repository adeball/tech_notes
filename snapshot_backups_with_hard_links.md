
# Snapshot backups with hard links and rsync

If no snapshot filesystem is available, it's possible to create "poor man's snapshot" using 
hard links and rsync, as in the following example:-

	### Rotate user backups

	BASE=/export/backup
	MAX=28
	### Rotate daily snapshots
	for location in users ; do
		echo
		echo "Syncing /home/e-smith/files/$location..."
		snap=$MAX
		while [[ $snap -ge 2 ]] ; do
			let nextsnap=$(($snap - 1))
			rm -rf $BASE/${location}.${snap} ### NB Only removes the oldest
			if [[ -d $BASE/${location}.${nextsnap} ]] ; then
				echo "Moving ${location}.${nextsnap} to ${location}.${snap}..."
				mv $BASE/${location}.${nextsnap} $BASE/${location}.${snap}
			fi
			let snap-=1
		done
		echo "Link/copying ${location}.0 to ${location}.1"
		cp -al $BASE/${location}.0 $BASE/${location}.1
		echo "Syncing /home/e-smith/files/${location}/ to ${location}.0"
		rsync -avx --delete --filter "- thumbnails/normal/*" --filter "- Cache/*" /home/e-smith/files/${location}/ $BASE/${location}.0
		touch $BASE/${location}.0
	done

	print "Backup completed: $(date)"


This needs some explanation, as it's not obvious (it exploits a feature of rsync).

Version .0 is an rsynced full copy of the source, and version .1 is a hard-linked 'copy' of version .0,
further hard-linked copies are created in rotation from version .1

In practice this means that version .1 is a set of hard links to version .0, *except* where file contents have
been changed or deleted.  In the case of change, version 0 gets a new inode.  In the case of deletions,
version .1 retains the original inode, which will have been removed in version .0 as it was synced with the
source.

More info here: http://earlruby.org/2013/05/creating-differential-backups-with-hard-links-and-rsync/comment-page-1/


From practical perspective, simply follow the recipe above and it's possible to replicate the usual daily,
weekly, monthly snapshot model often used on e.g. NetApp filer and ZFS.
