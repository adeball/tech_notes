Sort of a chicken and egg situation because you might find you don’t know the password when the server has crashed and you need the console, but let’s not go there and just assume you have access to a shell…

Prerequisite - hponcfg rpm 

vi /tmp/Password_ILO_reset.xml

<RIBCL VERSION="2.0">
<LOGIN USER_LOGIN="Administrator" PASSWORD="anytext">
<USER_INFO MODE="write">
<MOD_USER USER_LOGIN="Administrator">
<PASSWORD value="NewPassword"/>
</MOD_USER>
</USER_INFO>
</LOGIN>
</RIBCL>

hponcfg -f /tmp/Password_ILO_reset.xml

Administrator password now set to NewPassword

Originally from:

https://kumarvsphere.wordpress.com/2013/07/21/how-to-reset-hp-ilo-password-without-reboot-when-you-forgot-the-admin-root-password-esx-windows-linux/


