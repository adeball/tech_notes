
# Video

https://www.youtube.com/watch?v=_WOKRaM-HI4


# Installation of utilities

yum install setools setroubleshoot


# INFO

See /etc/selinux/config (symlink to /etc/sysconfig/selinux)

sestatus
getenforce


Standard policy is 'targetted'


List all types with: seinfo -t (if installed), or semanage fcontext -l, or sesearch (if installed)



# Labelling

Files & processes are labelled with their selinux context
	user:role:type:level(optional)

For non-MLS (i.e. most environments, MLS is a more advanced config) selinux only 'type' is important


Check the label of a file with ls -lZ

[root@host:~]# ls -lZ /usr/sbin/httpd
-rwxr-xr-x. root root system_u:object_r:httpd_exec_t:s0 /usr/sbin/httpd


Check the label of a process with ps -Z

[root@host:~]# ps -efZ | grep http | head -3
system_u:system_r:httpd_t:s0    root       2981      1  0 Jul19 ?        00:02:20 /usr/sbin/httpd -DFOREGROUND
system_u:system_r:httpd_t:s0    root       3256   2981  0 Jul19 ?        00:00:00 /usr/libexec/nss_pcache 7 off
system_u:system_r:httpd_t:s0    apache     3567   2981  0 Jul19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND



Even ports are labelled 

e.g. netstat -eZlpt
List ports, e.g.:  semanage port -l | grep http



# Type enforcement



# Booleans

Show all
getsebool -a
setsebool (0|1) -P for permanent

/etc/selinux/targeted/active/booleans.local lists what has been changed from default


# setroubleshoot

Install setroubleshoot & setroubleshoot-server (then restart auditd (NB: use service auditd restart, not systemctl))

# Logs

Try journalctl -b -0 (messages since last reboot)

# Change context

e.g. if files have been moved from a home dir, they will probably have the wrong context
e.g. chcon -t bin_t pathtofile
or you can use chcon --reference knowngoodfile pathtofile

restorecon - reads /etc/selinux, finds what the default context is and applies it
e.g. restorecon -rv (recursive, verbose)


Can use e.g. semanage-fcontext -e to set the equivalent context - e.g. to set a new web area to the same as /var/www/html, you can 
use semanage-fcontext -a -e /var/www/html /foo to store the new context settings under /etc/selinux
then restorecon -R -v /foo to actually set it


# sealert


# Create a new policy

e.g.
grep httpd /var/log/audit/audit.log | audit2allow -M mynewpolicy
semodule -i mynewpolicy.pp


# Set up tomcat

httpd_config_t
httpd_exec_t
httpd_log_t
httpd_sys_content_t
Set start script to this context ---> httpd_initrc_exec_t


# SystemV startup scripts with su - user

Replace su with 'runuser', it plays nicely with selinux




#---- real example ----

procmail failing to write to /data/local/username/Mail


Errors logged with scontext (source context I think) as procmail_t, target context default_t

## To list contexts procmail_t context processes can write to
sesearch -A -s procmail_t -p write -c file|grep procmail_t
and
sesearch -A -s procmail_t -p write -c dir|grep procmail_t
to list directories

Neither of these include the default_t context, so the target location context needs to be changed

e.g. to switch to user_home_t use the following:
semanage fcontext -a -t user_home_t /data/local/username
restorecon -RFvv /data/local/username


In practice, this failed to fix the fault, but no further errors were logged by auditd!!!

Apparently, there is a 'donotaudit' option which can filter out real errors, to switch this off, use
semodule -DB

Then look at /var/log/audit/audit.log again, the errors should now appear

Once the problem is sorted, this can be switched back on with: semodule -B


In this case, the problem was further up the directory tree, /data and /data/local were unlabelled.
Labelling these with a suitable context (which procmail is allowed to open, in this case home_root_t) solved the issue


