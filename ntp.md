
Not syncing.

Set 'tos maxdist 30' (or higher) to allow NTP to use "less reliable" sources to sync
(See: http://doc.ntp.org/4.2.6/miscopt.html)


See also http://log.or.cz/?p=80


cat /sys/devices/system/clocksource/clocksource0/available_clocksource 
cat /sys/devices/system/clocksource/clocksource0/current_clocksource

In one case acpi_pm was drifting veyr badly, but 'jiffies' was fine.


adjtimex can be used to tweak a fast/slow clock, as long as it is not way out (e.g. 9000 - 11000 for a 100Hz system)

ntpq> peers
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 tik.cesnet.cz   .GPS.            1 u   12   64  377    0.641  8494.05 2911.29
 tak.cesnet.cz   .GPS.            1 u    2   64  377    0.636  8594.86 2945.05


ntpq> as

ind assID status  conf reach auth condition  last_event cnt
===========================================================
  1 55713  9014   yes   yes  none    reject   reachable  1
  2 55714  9014   yes   yes  none    reject   reachable  1

Use the association ID above to examine a peer's stats in detail

ntpq> rv 55713
assID=55713 status=9014 reach, conf, 1 event, event_reach,
srcadr=tik.cesnet.cz, srcport=123, dstadr=195.113.20.142, dstport=123,
leap=00, stratum=1, precision=-20, rootdelay=0.000,
rootdispersion=0.000, refid=GPS, reach=377, unreach=0, hmode=3, pmode=4,
hpoll=6, ppoll=6, flash=400 peer_dist, keyid=0, ttl=0, offset=13041.231,
delay=0.602, dispersion=0.944, jitter=2918.331,
reftime=cf803b51.ddd3e70e  Mon, Apr 26 2010 18:18:25.866,
org=cf803b83.e9b29181  Mon, Apr 26 2010 18:19:15.912,
rec=cf803b76.df382c7c  Mon, Apr 26 2010 18:19:02.871,
xmt=cf803b76.df0d40c7  Mon, Apr 26 2010 18:19:02.871,
filtdelay=     0.60    0.64    0.60    0.51    0.82    0.67    0.69    0.64,
filtoffset= 13041.2 12385.8 11720.4 11075.2 10409.6 9774.54 9129.22 8494.06,
filtdisp=      0.00    0.98    1.97    2.93    3.92    4.86    5.82    6.77
