
### Start

git init

### Useful Git log alias
alias gl='git log --oneline --abbrev-commit --all --graph --decorate --color'

### References

* http://think-like-a-git.net/sections/testing-out-merges/the-savepoint-pattern.html

### Github remote 
* Show: git remote -v
* Set push (e.g.): git remote set-url --push origin ssh://git@github.com/adeball/tech_notes.git (NB: Always use git@github.com, not the normal username.  Copy id_rsa.pub as a deploy key for the repo).


#### Push branch to remote

git checkout -b newbranch
git push -u origin newbranch (or git push --set-upstream origin newbranch)


#### Delete branch on remote 

##### Deleting a remote branch

git push origin --delete <branch>  # Git version 1.7.0 or newer

##### Deleting a local branch

git branch --delete <branch>

##### Deleting a local remote-tracking branch

git branch --delete --remotes <remote>/<branch>
git fetch <remote> --prune # Delete multiple obsolete remote-tracking branches





### Show all files changed in the last 3 days e.g.
git diff --name-only "@{3 days ago}"
(omit the --name-only to see the changes)


### Remove a file and all traces from repo (e.g. if it has sensitive data)

* cd top-level-of-repo copy
* git pull
* If you want to keep the latest version - i.e. it's removed of sensitive data, copy it to a new name  - otherwise the file is removed completely.
* git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch path/to/sensitive_file' --prune-empty --tag-name-filter cat -- --all
* git push origin --force --all
* You can then rename the clean copy to the original name and commit/push again

(See: https://help.github.com/articles/removing-sensitive-data-from-a-repository/)


### See diff for latest commit

git diff HEAD HEAD^
(or specify the commit hashes)


### Process for MO Puppet

1. git checkout -b coolfeature
2. Make updates to files
3. Update CHANGELOG.md
4. Update Metadata.json (match version number)
5. git commit -a -m "commit message"
6. git push origin coolfeature
7. Logon to Github server, select repo
8. Create pull request, select required branch
9. Wait for approval
10. Merge (delete orig branch)
11. On the master branch, create tag with the same name as the version number (e.g. git tag 1.2.3)
12. Update version number in control repo Puppetfile 
13. Create pull request (describe as e.g. 'Bump c_mymodule to 1.2.3')
14. Wait for approval
15. Merge (delete orig branch) 
16. Run control/production build


### Tags

git tag -a 0.0.7 -m "My message"
git push origin --tags


### Logs/changes

git log --graph --abbrev-commit --decorate
git log --follow -p -- files/etc/sudoers


### Fetch a file from the repo

git fetch --all
git checkout origin/main -- thefileinquestion
