### Some commands and places to look for logs
### Based on one troubleshooting exercise

# Places

* /opt/OV
* /opt/perf
* /var/opt/OV
* /var/opt/perf


# Useful commands

* /opt/perf/bin/perfstat -p
* /opt/OV/bin/ovc -status/stop/start/restart etc (OV control?)
* /opt/OV/bin/ovc -stop/start coda 
* /opt/perf/bin/ovpa -stop/start (perf daemons)
* /opt/OV/bin/opcagt -status


# Logs

/var/opt/perf/status.perfalarm (other files in here too)

# perfstat shows perfalarm not active
Checking the /var/opt/perf/status.perfalarm file revealed the following error: “scope/dsi log file initialization failed”
Looking this up, led me to check the /var/opt/OV/conf/perf/datasources file on this and a working system.  
This system was missing the line for the logglob file (DATASOURCE=SCOPE LOGFILE=/var/opt/perf/datafiles/logglob)

Added this in, stopped/killed/restarted and it all looks like it’s working now.


## Expected perfstat output


'''
hostname root /var/opt/perf # /opt/perf/bin/perfstat -p
**********************************************************
*** perfstat for hostname.domain.local on Thu Oct  6 11:55:28 BST 2016 
*** Linux hostname.domain.local 2.6.32-573.1.1.el6.x86_64 #1 SMP Tue Jul 14 02:46:51 EDT 2015 x86_64 x86_64 x86_64 GNU/Linux 
**********************************************************

list of performance tool processes:
----------------------------------

 Perf Agent status:
    Running scopeux               (Perf Agent data collector) pid 8913 
    Running midaemon              (Measurement Interface daemon) pid 8917
    Running ttd                   (ARM registration daemon) pid 8899 

 Perf Agent Server status:

    Running ovcd                  (OV control component) pid 9403 
    Running ovbbccb               (BBC5 communication broker) pid 9411 
    Running coda                  (perf component) pid(s) 9485 
       Configured DataSources(5)
                  SCOPE
                  ESARINV
                  SYSDOWNT
                  DISKCAPACITY
                  DISKSIZE

    Running perfalarm             (alarm generator) pid(s) 8922 
OV Operation Agent status:
coda        OV Performance Core                 COREXT       (9485)   Running 
opcacta     OVO Action Agent                    AGENT,EA     (9469)   Running 
opcle       OVO Logfile Encapsulator            AGENT,EA     (9491)   Running 
opcmona     OVO Monitor Agent                   AGENT,EA     (9459)   Running 
opcmsga     OVO Message Agent                   AGENT,EA     (9500)   Running 
opcmsgi     OVO Message Interceptor             AGENT,EA     (9480)   Running 
ovbbccb     OV Communication Broker             CORE         (9411)   Running 
ovcd        OV Control                          CORE         (9403)   Running 
ovconfd     OV Config and Deploy                COREXT       (9438)   Running 


************* (end of perfstat -p output) ****************
'''
