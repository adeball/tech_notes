
# MySQL 101

## Connect
```
mysql -u user -p database
mysql -u user --password='password' database
```

## Basic
```
show databases;
use <database>;
show tables;
describe <table>;  show columns in <table>;
```

## Backup
```
mysqldump -u user -p database > database.sql
```

## Restore
```
mysql> drop database <database>;
mysql> create database <database>;
mysql -u ... databasename < backupfilename.sql
```

## Users

### Create a user 'myuser' and allow all access to 'mydatabase' from the local system

create user myuser;
grant all privileges on MYDATABASE.* to 'myuser'@'localhost' identified by 'mypassword';
grant all privileges on MYDATABASE.* to 'myuser'@'myhost' identified by 'mypassword';
grant all privileges on MYDATABASE.* to 'myuser'@'myhost.mydomain' identified by 'mypassword';

### Remove a user

drop user myuser;

### e.g. Find the last ten logins ordered by date
select id,username,lastlogin,lastaccess,ctime from ep_usr order by lastlogin desc limit 10;

