
## Display

klist


## Initialise

kinit


## Create keytab example

cd 
ktutil
  ktutil:  addent -password -p myusername@KERBEROS.REALM.COM -k 1 -e aes256-cts
  Password for myusername@KERBEROS.REALM.COM: [enter your password]
  ktutil:  wkt .username.keytab
  ktutil:  quit

## Initialise with keytab

kinit myusername@KERBEROS.REALM.COM -k -t ~/.username.keytab
