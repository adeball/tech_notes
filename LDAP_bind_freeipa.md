
Notes from a working set up of an appliance which needed to authenticate to a FreeIPA server (v2.2)

## Requirements / assumptions

* Assume FreeIPA server is ipaserver.fqdn.co.uk
* Normal user account is myusername
* Priv group (for example) is privgroup
* System to connect to IPA is myusersystem
* You have the FreeIPA directory manager user password (or equivalent)

* Cert from /etc/ipa/ca.crt on ipaserver
 

dc=fqdn,dc=co,dc=uk
Username attribute: uid
User filter: (objectclass=inetuser)
Display Name Attribute: displayName

Filter example: (&(objectClass=posixGroup)(memberUid=%s)(|(cn=privgroup)(cn=myusersystem_admin)))


## Set up bind user, if needed

### On ipaserver

Create myusersystem.ldif

	dn: uid=myusersystembind,cn=sysaccounts,cn=etc,dc=fqdn,dc=co,dc=uk
	changetype: add
	objectclass: account
	objectclass: simplesecurityobject
	uid: myusersystembind
	userPassword: enteratextpasswordhere
	passwordExpirationTime: 20380119031407Z
	nsIdleTimeout: 0

Then

	ldapmodify -h ipaserver.fqdn.co.uk -p 389 -x -D "cn=Directory Manager" -W -f myusersystem.ldif
(Directory Manager password should be known from the freeipa setup stage)


## Working ldapsearch examples

### Anon bind

	ldapsearch -x -H ldap://localhost:389 -b dc=fqdn,dc=co,dc=uk "(&(objectclass=inetuser)(uid=myusername))"

### Non-TLS bind with user name

	ldapsearch -x -H ldap://localhost:389 -w "myusersystembind_passwd" -D "uid=myusersystembind,cn=sysaccounts,cn=etc,dc=fqdn,dc=co,dc=uk" -b "dc=fqdn,dc=co,dc=uk" "(&(objectclass=inetuser)(uid=myusername))"

### TLS bind with user name

	LDAPTLS_CACERT=/etc/ipa/ca.crt (wget from same loc on ipaserver, if you don't have it)
	export LDAPTLS_CACERT
	ldapsearch -ZZ -x -H ldap://localhost:389 -w "myusersystembind_password" -D "uid=myusersystembind,cn=sysaccounts,cn=etc,dc=fqdn,dc=co,dc=uk" -b "dc=fqdn,dc=co,dc=uk" "(&(objectclass=inetuser)(uid=myusername))"


## Configure IPA client on Linux

	DOMAIN=domain.co.uk
	USER=myipauser			# Needs privs
	SERVER=myipaserver.$DOMAIN
	HOST=myhost.$DOMAIN

	ipa-client-install -p $USER --no-ntp --mkhomedir --no-dns-sshfp --server=$SERVER --domain=$DOMAIN --hostname=$HOST

## Unlock a user account

1. Verify the user has been locked out (krbloginfailedcount will be at the policy limit, 4):

    $ ipa user-show bob --all|grep krbloginfailedcount
      krbloginfailedcount: 4

2. Unlock the account using ipa user-unlock:

    $ ipa user-unlock bob

### Alternate method (on IPA server)

1. Login as root
2. kadmin.local
3. getprinc username (look for #failed password attempts, if it's at the max, the account is locked)
4. modprinc -unlock username
5. getprinc username again to confirm

