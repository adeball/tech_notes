

1. Works on a current working directory basis (need to check what happens with two+ files) - so any .tf files in the current directory are parsed.
2. Create a file - e.g. like the "helloworld.tf" in this dir.  It works fine with a ~/.aws directory for the access key id and secret access keys for AWS.  You still need to tell it the region, even though it might be configured in ~/.aws

3. Use "terraform plan" to see what it will do

4. Use "terraform apply" to do it

5. Use "terraform destroy" to delete stuff


* Remote state storage

Rather than keeping local files (i.e. terraform.tfstate in the local directory), you can use "Atlas" (https://atlas.hashicorp.com/) to do this.
1. Sign up
2. Ignore the charged-for enterprise stuff top-left.  Go to your account name (bottom-left), then "account settings"
3. Use the "Tokens" option to generate a token

Hmmmm... following the instructions at: https://www.terraform.io/intro/getting-started/remote.html fails - as it seems to require Atlas Enterprise.



* Dependencies

Cool to use graphviz :-)
e.g.

terraform graph | dot -Tpng > /tmp/a.png ; open /tmp/a.png
