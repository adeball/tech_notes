#!/bin/bash
#
# Create any groups which don't yet exist on the new satellite
# Run once at the start of the migration
#

cd $(dirname $0); . ./common.sh


ssh $OLDSAT "spacecmd -q group_list" > group_list.$OLDSAT

while read group ; do
    echo $group
    ssh -n $NEWSAT "echo $group | spacecmd -q group_create $group"
done < group_list.$OLDSAT

