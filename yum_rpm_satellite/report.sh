#!/bin/bash

cd $(dirname $0) ; . ./common.sh

if [[ $# -eq 1 ]] ; then

    ### Host name provided, run verbose report on that host

    HOST=$1
    cd hosts/$HOST || fail "Cannot cd hosts/$HOST"

    echo
    echo "============================================================================"
    echo "Data from satellite servers for $HOST"
    echo

    width=64 # Width of one column
    fmt="%-${width}s%-${width}s\n"
    printf "$fmt" "Old satellite ($OLDSAT)"      "New satellite ($NEWSAT)"
    printf "$fmt" "----------------------------" "-----------------------------"

    params=$(ls | sed -e 's/\..*$//' | sort -u)

    for param in $params ; do
        info "$param"
        [[ -f $param.$OLDSAT ]] && f1=$param.$OLDSAT || f1=/dev/null
        [[ -f $param.$NEWSAT ]] && f2=$param.$NEWSAT || f2=/dev/null
        sdiff -w $(( $width * 2 + 2 )) $f1 $f2
    done

    echo

else

    ### No hostname provided, run summary
    ### Ignore activation key differences

    cd hosts
    for host in $(ls) ; do
        diffs=""
        params=$(ls $host | sed -e 's/\..*$//' | sort -u | egrep -v "activationkeys|checkin")
        for param in $params ; do
            [[ -f $host/$param.$OLDSAT ]] && f1=$host/$param.$OLDSAT || f1=/dev/null
            [[ -f $host/$param.$NEWSAT ]] && f2=$host/$param.$NEWSAT || f2=/dev/null
            diffs="$diffs$(sdiff -s $f1 $f2 | grep -v null)"
        done

        status=""

        if [[ -f $host/groups.$NEWSAT && ! -f $host/donotmove ]] ;then
            location="*   Migrated"
        else
            location="    Unmoved"
            if [[ -f $host/mig.log ]] ; then
                grep -q "LOW ON SPACE" $host/mig.log && status="Filesystem low on space"
                grep -q "check routing" $host/mig.log && status="Comms failure"
                grep -q "Cannot ssh" $host/mig.log && status="Cannot ssh"
                grep -q "Registration failed" $host/mig.log && status="Registration failed"
                egrep -q "^$(date +%Y)" $host/checkin.$OLDSAT
                [[ $? -ne 0 ]] && status="$status (last check-in $(sed -e 's/T.*//' $host/checkin.$OLDSAT))"
                [[ -f $host/onvsphere ]] && status="$status - exists in VSphere"

                ### Flag up if hostname implies it's a VM, and it doesn't exist
                if [[ -f $host/notonvsphere ]] ;then
                    echo $host | egrep -q "^exv|^exxv"
                    [[ $? -eq 0 ]] && status="$status - Does not exist in VSphere"
                fi
            else
                status="No log output..."
            fi
            if [[ -f $host/failreason ]] ; then
                [[ -s $host/failreason ]] && status=$(head -1 $host/failreason)
                location="-   Pending"
            fi
            [[ -f $host/donotmove ]] && location="=   Do not move"
        fi

        if [[ ! -f $host/donotmove ]] ; then
            if [[ -z "$diffs" ]] ;then
                status="Identical"
            else
                if [[ "$location" == "*   Migrated" ]] ; then
                    diffchars=$(echo $diffs | wc -c)
                    if [[ $diffchars -lt 100 ]] ; then
                        status="Different - minimal"
                    elif [[ -f $host/checked ]] ; then
                        status="Different - confirmed ok"
                    else
                        status="MANY DIFFERENCES ($diffchars chars) - CHECK"
                    fi
                fi
            fi
        fi

        [[ ! -f $host/groups.$OLDSAT ]] && status="No data on $OLDSAT"

        printf "%-50s %-16s %-10s\n" $host "$location" "$status"
    done

fi
