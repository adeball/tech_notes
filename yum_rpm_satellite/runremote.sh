#!/bin/bash

cd $(dirname $0) ; . ./common.sh

HOST=$1 ; SCRIPT=$2

HAVEROOT=0
id=$(ssh -nq -o passwordAuthentication=no -o connectTimeout=5 $HOST "id -u")
[[ $? -ne 0 ]] && fail "Cannot ssh $HOST"
[[ $id -eq 0 ]] && HAVEROOT=1   # Set if we have direct root ssh

info "Copying $SCRIPT script to $HOST"
scp $SCRIPT $HOST:/tmp

info "Switching satellite servers on $HOST"
if [[ $HAVEROOT == 0 ]] ; then
    if [[ -z "$CDNIPAPASS" ]] ; then
        ssh -tt $HOST "sudo /bin/bash /tmp/$SCRIPT"
    else
        stty -echo
        echo "$CDNIPAPASS" | ssh -tt $HOST "sudo -S /bin/bash /tmp/$SCRIPT"
        stty echo
    fi
else
    ssh -n $HOST "/bin/bash /tmp/$SCRIPT"
fi

