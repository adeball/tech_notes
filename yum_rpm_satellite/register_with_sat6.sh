#!/bin/bash

cd /etc/sysconfig/rhn
mv systemid systemid.$(date +%F)
### Modify for environment
### Note: Org might use the label (see 'hammer organization list')
subscription-manager register --activationkey="RHEL7_Live" --org My_Organisation --force
yum clean all
yum repolist
service rhnsd restart


### Check for katello-ca-consumer-latest package if there are cert problems 
### (pull from https://satserver/pub/katello-ca-consumer-latest-noarch.rpm)
