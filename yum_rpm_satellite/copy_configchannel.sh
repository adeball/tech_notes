#!/bin/bash

OLDSAT=exxrhn
NEWSAT=exvrhnsat5

if [[ $# -ne 1 ]] ; then 
	echo "Usage: $0 channel"
	exit 1
fi

channel=$1

cd $(dirname $0)
mkdir -p config_channels
ssh -n $OLDSAT "cd /tmp; rm -f ${channel}.json ; spacecmd -q configchannel_export $channel"
scp $OLDSAT:/tmp/${channel}.json config_channels
scp config_channels/${channel}.json $NEWSAT:/tmp
ssh -n $NEWSAT "spacecmd -q configchannel_import /tmp/${channel}.json"


