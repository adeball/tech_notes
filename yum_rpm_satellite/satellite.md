
# Satellite 5

Server is configured in: /etc/sysconfig/rhn/up2date

Deploy a file: rhncfg-client get /path/to/file
Check a file: rhncfg-client verify /path/to/file


## taskomatic RAM

### /usr/share/rhn/config-defaults/rhn_taskomatic_daemon.conf
```
# Initial Java Heap Size (in MB)
wrapper.java.initmemory=2048

# Maximum Java Heap Size (in MB)
wrapper.java.maxmemory=6144
```


### /etc/sysconfig/tomcat6
```
JAVA_OPTS="-ea -Xms1532m -Xmx8192m -Djava.awt.headless=true -Dorg.xml.sax.driver=org.apache.xerces.parsers.SAXParser -Dorg.apache.tomcat.util.http.Parameters.MAX_COUNT=1024 -XX:MaxNewSize=256 -XX:-UseConcMarkSweepGC -Dnet.sf.ehcache.skipUpdateCheck=true -Djavax.sql.DataSource.Factory=org.apache.commons.dbcp.BasicDataSourceFactory"
```
### /etc/rhn/rhn.conf
```
taskomatic.maxmemory=12000
taskomatic.errata_cache_max_work_items = 300
taskomatic.channel_repodata_max_work_items = 30
taskomatic.channel_repodata_workers = 4
```

* Check /var/log/rhn/rhn_taskomatic_daemon.log for out of memory etc
* Restart taskomatic

