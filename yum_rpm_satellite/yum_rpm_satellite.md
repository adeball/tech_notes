
### History

#### recent systems
yum history
#### old (RHEL5 etc)
rpm -qa --last


### Config

/etc/sysconfig/rhn/up2date


### Commands

yum --showduplicates ...  e.g. yum list available --showduplicates git
yum whatprovides ...


