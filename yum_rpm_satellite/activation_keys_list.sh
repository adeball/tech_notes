#!/bin/bash

cd $(dirname $0)
. ./common.sh

for sat in $OLDSAT $NEWSAT ; do
    info "Generating list of defined activation keys on $sat"
	ssh $sat "spacecmd -q activationkey_list" > activation_key_list.$sat
done
