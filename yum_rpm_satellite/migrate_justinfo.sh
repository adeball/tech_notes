#!/bin/bash

cd $(dirname $0) ; . ./common.sh

[[ $# -ne 1 ]] && fail "Usage: $0 hostname"

HOST=$1


### Work on sat servers

info "Migrating information for $HOST from $OLDSAT to $NEWSAT"

if [[ ! -d hosts/$HOST ]] ; then
	echo "hosts/$HOST does not exist, have you run ./audit.sh $HOST ?"
	exit 1
fi

### Spacecmd is flaky, this is often needed
info "Clearing caches"
ssh -n $NEWSAT spacecmd -q clear_caches


### Check that the hosts exists with the same name (might not be FQDN)
### If not, delete/re-add with the FQDN

# spacecmd does return an error code when querying something non-existent
info "Checking $HOST is now configured on $NEWSAT"
if [[ $(existsonsat $HOST $NEWSAT) == 0 ]] ; then
    fail "$HOST does not exist on $NEWSAT"
fi

### Software channels

info "Setting base channel"
while read channel ; do
    echo "Base channel=$channel"
    ssh -n $NEWSAT "spacecmd system_setbasechannel $HOST $channel -y"
done < hosts/$HOST/basechannel.$OLDSAT

# Do one at once in case any are missing
info "Subscribing to child channels"
while read channel ; do
    echo "Child channel=$channel"
    ssh -n $NEWSAT "spacecmd system_addchildchannels $HOST $channel -y"
done < hosts/$HOST/childchannels.$OLDSAT

### Config channels

info "Migrating software channels"
while read channel ; do
    echo "Channel: $channel"
    ssh -n $NEWSAT "echo b | spacecmd -q system_addconfigchannels $HOST $channel"
    echo
done < hosts/$HOST/configchannels.$OLDSAT


### Groups
info "Migrating groups"
while read group ; do
    echo "$group"
    ssh -n $NEWSAT "spacecmd -q group_addsystems $group $HOST"
done < hosts/$HOST/groups.$OLDSAT


### Activation Keys

info "Activation keys"
## This is done on registration...

### Custom Values
info "Copying custom values"

while read key equals value ; do
  [[ -z "$value" ]] && value="null"
  echo "$key=$value"
  ssh -n $NEWSAT "spacecmd -q system_addcustomvalue $key \"$value\" $HOST"
done < hosts/$HOST/customvalues.$OLDSAT

### Clear caches, otherwise stuff gets out of date
ssh -n $NEWSAT "spacecmd -q clear_caches"

### Update audit data from new satellite
. ./audit.sh -n $HOST
