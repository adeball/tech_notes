#!/bin/bash

cd $(dirname $0) ; . ./common.sh

if [[ $# -ne 1 ]] ; then 
	echo "Usage: $0 key"
	exit 1
fi

key=$1

mkdir -p activation_keys
ssh $OLDSAT "cd /tmp; rm -f $key.json; spacecmd -q activationkey_export $key"
scp $OLDSAT:/tmp/${key}.json activation_keys
scp activation_keys/${key}.json $NEWSAT:/tmp
ssh $NEWSAT "spacecmd -q activationkey_import /tmp/${key}.json"


echo
echo "If this errored with 'invalid channel', check that all the channels listed in /tmp/$key.json exist on $NEWSAT"
echo
