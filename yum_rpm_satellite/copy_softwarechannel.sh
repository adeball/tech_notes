#!/bin/bash

cd $(dirname $0) ; . ./common.sh

if [[ $# -ne 1 ]] ; then 
	echo "Usage: $0 channel"
	exit 1
fi

channel=$1

cd $(dirname $0)
mkdir -p software_channels
ssh $OLDSAT "cd /tmp; rm -f ${channel}.json ; spacecmd -q softwarechannel_export $channel"
scp $OLDSAT:/tmp/${channel}.json software_channels
scp config_channels/${channel}.json $NEWSAT:/tmp
ssh $NEWSAT "spacecmd -q softwarechannel_import /tmp/${channel}.json"


