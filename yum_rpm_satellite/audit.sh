#!/bin/bash
#
# audit.sh
#
# Queries satellite server(s) for current values for channels, groups, keys, custom values
# and stores in the hosts/<host> directory
#
# Usage: audit.sh [-o] [-n] hostname
# -o Query old sat server only
# -n Query new sat server only
# No switch = query both
#

cd $(dirname $0)
. ./common.sh


SATSERVERS="$OLDSAT $NEWSAT"
while getopts "on" opt; do
        case $opt in
                o)
                        [[ "$SATSERVERS" == "$NEWSAT" ]] && fail "Only specify one of -o or -n"
                        SATSERVERS="$OLDSAT"
                        shift $(( $OPTIND - 1 ))
                        ;;
                n)
                        [[ "$SATSERVERS" == "$OLDSAT" ]] && fail "Only specify one of -o or -n"
                        SATSERVERS="$NEWSAT"
                        shift $(( $OPTIND - 1 ))
                        ;;
                \?)
                        fail "Invalid option: Usage: $0 [-o] [-n] hostname"

                        ;;
        esac
done

[[ $# -eq 1 ]] || fail "Usage: $0 [-o] [-n] hostname"
HOST=$1

info  "            : Auditing $HOST
Satellite server(s): $SATSERVERS
Output             : ./hosts/$HOST/*"


[[ ! -d hosts ]] && mkdir -p hosts

for sat in $SATSERVERS ;do

    if [[ $(existsonsat $HOST $sat) == 1 ]] ; then

        # Marginally inefficient, but means it's not created if the host is not found on either sat server
        mkdir -p hosts/$HOST

        info "Software channels ($sat)"

        ssh -n $sat "spacecmd -q system_listbasechannel $HOST" | sort | tee hosts/$HOST/basechannel.$sat
        ssh -n $sat "spacecmd -q system_listchildchannels $HOST" | sort | tee hosts/$HOST/childchannels.$sat


        info "Config channels ($sat)"

        ssh -n $sat "spacecmd -q system_listconfigchannels $HOST" | sort | tee hosts/$HOST/configchannels.$sat


        info "Groups ($sat)"

        grep $HOST groups.$OLDSAT/* | sed -e 's-.*/--;s/:.*//' | sort | tee hosts/$HOST/groups.$sat


        info "Custom Values ($sat)"

        ssh -n $sat "spacecmd -q system_listcustomvalues $HOST" | sort | tee hosts/$HOST/customvalues.$sat


        info "Activation Keys ($sat)"

        tmpf=$(mktemp)

        ssh -n $sat "spacecmd -q system_details $HOST" > $tmpf
        awk 'act==1 && /^[0-9a-zA-Z]/ {print} /^$/ {act=0} /Activation/ {act=1}' $tmpf \
            | sort | tee hosts/$HOST/activationkeys.$sat

        info "Last check-in ($sat)"
        cdate=$(awk '/Last Checkin/ {print $3}' $tmpf | sed -e 's/T/ /') 
	cdate=$(date -d "$cdate" +"%F %T")
	echo "Last Check-in: $cdate" | tee hosts/$HOST/checkin.$sat

        rm $tmpf

    else
        echo " -- $HOST does not exist on $sat"
    fi

done

exit
### This file should be there, as there's no point auditing anything on just the new sat server
### but then, people do strange things, so check anyway.
if [[ -f "hosts/$HOST/activationkeys.$OLDSAT" ]] ; then

    info "Checking activation keys on $OLDSAT exist on $NEWSAT"
    newsatactkeylist=activation_key_list.$NEWSAT
    newsatmissingkeylist=activation_key_list_missing.$NEWSAT
    [[ ! -f $newsatactkeylist ]] && ssh -n $NEWSAT "spacecmd -q activationkey_list" > $newsatactkeylist
    while read actkey ; do
        egrep -q "^$actkey$" $newsatactkeylist
        if [[ $? -eq 0 ]] ; then
            echo "Got $actkey"
        else
            echo "  --- FAIL --- $actkey not on $NEWSAT"
            echo $actkey >> $newsatmissingkeylist
        fi
    done < hosts/$HOST/activationkeys.$OLDSAT

fi

echo
