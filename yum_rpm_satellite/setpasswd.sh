#!/bin/bash
#
# Source this to set your CDN IPA passwd for auto sudo as root
#
# i.e.
# . ./setpasswd.sh
#


if [[ -z "$CDNIPAPASS" ]] ; then
        echo -n "Enter CDN password: "
        export CDNIPAPASS=$(read -s p; echo $p)
        stty echo
	echo
fi

