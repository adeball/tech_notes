#!/bin/bash
#
# common.sh
#
# Source this for common values/functions
#
# e.g. cd $(dirname $0) ; . ./common.sh
#

fail()
{

    echo >&2
    echo "Script failed: $@" >&2
    echo >&2
    exit 1

}


info()
{
    echo
    echo " ----- $@"
}


existsonsat()
{
    if [[ $# -ne 2 ]] ; then
        echo "Usage: existsonsat host satellite"
        return
    fi
    host=$1
    sat=$2
    details=$(ssh -n $sat "spacecmd -q system_details $host")
    [[ -n "$details" ]] && echo 1 || echo 0
}


existsonvsphere()
{
   if [[ $# -ne 1 ]] ; then
        echo "Usage: existsonvsphere host"
        return
    fi
    host=$1
    shortname=${host%%.*}
    longname=${shortname}.fqdn.uk

    ### For 99% of systems, one of the above options should match.  If not, check manually...
    vs=$(ssh -n $VSPHERE "cd /home/linux/perl_scripts; ./search_vsphere.pl --vmname $shortname ; ./search_vsphere.pl --vmname $longname" 2>/dev/null)
    [[ -n "$vs" ]] && echo 1 || echo 0

}


export OLDSAT=oldsat
export OLDSATFULL=oldsat.fdqn.uk
export OLDSATIP=10.0.0.1

export NEWSAT=newsat
export NEWSATFULL=newsat.fqdn.uk
export NEWSATIP=10.0.0.2

export REPOHOST=repohost.fdqn.uk
export REPOHOSTIP=10.0.0.3

export VSPHERE=mgzvma01
