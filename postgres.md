
### Connect to database

psql -U username -d dbname (if no dbname, try 'postgres')


### Show databases

\list (or \l)


### Connect when in psql

\connect (or \c)


### Show tables

\dt (or SELECT * FROM pg_catalog.pg_tables;)


### Export

pg_dump dbname (might need -U username)

### Import

psql databasename < data_base_dump (might need -U username)


