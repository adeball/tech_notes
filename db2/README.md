* Database users are defined by the OS (i.e credentials are OS creds, however that is done).
* Usernames and DB names have an 8 character limit
* Windows/Linux DBs are not compatible.  Check db2move/db2look: https://technomagus.wordpress.com/database/ibm-db2/transfer-db2-database-from-linux-to-windows/


#Remote database

To connect to a remote database, you need to add it to the database catalog, specifying the name of the remote node.
The remote node name is not looked up outside DB2, it needs first adding to the node directory.

e.g.

* db2 
* list node directory (to see what's already created)
* catalog tcpip node myremoteservername remote 10.0.0.1 server 50005  (where myremoteservername is an arbitrary name of your choice, 10.0.0.1 is the remote server IP, and 50005 is the remote listener tcp port)
* catalog database myremotedb as myremotedbalias at node myremoteservername


#Copy to/from Linux to Windows

A backup/restore won't work due to the endian difference (what, are we in the 1970s!?) - you can use db2move/db2look to export/import instead.

e.g.

db2move GTRMXTST export -aw -l lobs -sn MAXIMO -u maximo -p mypasswd
db2look -d GTRMXTST -a -e -x -i maximo -o GTRMXTST_maximo.ddl -w mypasswd

db2move newdbname import -io replace_create -l lobs -u maximo -p mypasswd
Edit the ddl file used in the next statement, and change the database name near the top to 'newdbname' if necessary
NB: You can also add the password with e.g. "user myusername using mypassword"
db2 -tvf GTRMXTST_maximo.ddl


#Database federation

https://www.ibm.com/developerworks/community/blogs/db2luwtechsupport/entry/how_to_setup_federation_between_two_db2_luw_databases2?lang=en


db2 connect to datavaul
db2 update dbm cfg using federated yes

db2stop [force if needed]
db2start

db2 get dbm cfg | grep -i federated


db2level (to see versions)
db2 

db2 => create server maximo_db type db2/udb version 10.5.4 wrapper drda authorization "maximo" password "thedbpassword" options(add DBNAME 'maximo')
DB20000I  The SQL command completed successfully.

db2 => create user mapping for datavaul server maximo_db options(remote_authid 'maximo', remote_password 'thedbpassword')
DB20000I  The SQL command completed successfully.


### Drop a database

CONNECT TO XYZ;
UNQUIESCE DATABASE;
QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS;
UNQUIESCE DATABASE;
CONNECT RESET;
deactivate db XYZ;
DROP DATABASE XYZ;
