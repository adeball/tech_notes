
I have set the HADR read only flags, and restarted the standby instance:
as part of this I also changed the HADR parameters from servernames to the db2 alias hostnames
db2-001.prd.rwg.eams.cloud
db2-002.prd.rwg.eams.cloud

on both prod and standby

Commands I ran on the standby:

sudo su - ctginst1
db2 deactivate rwgmxprd
db2 stop HADR on db rwgmxprd

 ## RWG standby instance
#Set the hadr read flags
db2set DB2_HADR_ROS=ON
db2set DB2_STANDBY_ISO=UR
#restart the db
db2stop
db2start
# reset the hadr parameters to reflect the db2 aliases instead of the actual hostname
db2 update db cfg for rwgmxprd using HADR_LOCAL_HOST db2-002.prd.rwg.eams.cloud 
db2 update db cfg for rwgmxprd using HADR_LOCAL_SVC CTGINST1_HADR_2 
db2 update db cfg for rwgmxprd using HADR_REMOTE_HOST  db2-001.prd.rwg.eams.cloud 
db2 update db cfg for rwgmxprd using HADR_REMOTE_SVC CTGINST1_HADR_1 
db2 update db cfg for rwgmxprd using HADR_REMOTE_INST CTGINST1 


db2 start hadr on database rwgmxprd as standby 

After the change as you can see the HADR successfully restarted and the standby caught on:

Primary:

[ctginst1@barquentine ~]$ db2pd -hadr -db rwgmxprd

Database Member 0 -- Database RWGMXPRD -- Active -- Up 160 days 01:33:23 -- Date 2017-11-16-15.11.47.964774

                            HADR_ROLE = PRIMARY
                          REPLAY_TYPE = PHYSICAL
                        HADR_SYNCMODE = NEARSYNC
                           STANDBY_ID = 1
                        LOG_STREAM_ID = 0
                           HADR_STATE = PEER
                           HADR_FLAGS =
                  PRIMARY_MEMBER_HOST = db2-001.prd.rwg.eams.cloud
                     PRIMARY_INSTANCE = ctginst1
                       PRIMARY_MEMBER = 0
                  STANDBY_MEMBER_HOST = db2-002.prd.rwg.eams.cloud
                     STANDBY_INSTANCE = ctginst1
                       STANDBY_MEMBER = 0
                  HADR_CONNECT_STATUS = CONNECTED
             HADR_CONNECT_STATUS_TIME = 11/16/2017 15:11:10.640036 (1510845070)
          HEARTBEAT_INTERVAL(seconds) = 30
                     HEARTBEAT_MISSED = 0
                   HEARTBEAT_EXPECTED = 0
                HADR_TIMEOUT(seconds) = 120
        TIME_SINCE_LAST_RECV(seconds) = 3
             PEER_WAIT_LIMIT(seconds) = 0
           LOG_HADR_WAIT_CUR(seconds) = 0.000
    LOG_HADR_WAIT_RECENT_AVG(seconds) = 0.000511
   LOG_HADR_WAIT_ACCUMULATED(seconds) = 26241.013
                  LOG_HADR_WAIT_COUNT = 51273567
SOCK_SEND_BUF_REQUESTED,ACTUAL(bytes) = 0, 332800
SOCK_RECV_BUF_REQUESTED,ACTUAL(bytes) = 0, 235416
            PRIMARY_LOG_FILE,PAGE,POS = S0001606.LOG, 3131, 26866167501
            STANDBY_LOG_FILE,PAGE,POS = S0001606.LOG, 3130, 26866164412
                  HADR_LOG_GAP(bytes) = 0
     STANDBY_REPLAY_LOG_FILE,PAGE,POS = S0001606.LOG, 3130, 26866164295
       STANDBY_RECV_REPLAY_GAP(bytes) = 117
                     PRIMARY_LOG_TIME = 11/16/2017 15:11:44.000000 (1510845104)
                     STANDBY_LOG_TIME = 11/16/2017 15:11:40.000000 (1510845100)
              STANDBY_REPLAY_LOG_TIME = 11/16/2017 15:11:39.000000 (1510845099)
         STANDBY_RECV_BUF_SIZE(pages) = 2048
             STANDBY_RECV_BUF_PERCENT = 0
           STANDBY_SPOOL_LIMIT(pages) = 1048576
                STANDBY_SPOOL_PERCENT = 0
                   STANDBY_ERROR_TIME = NULL
                 PEER_WINDOW(seconds) = 0
             READS_ON_STANDBY_ENABLED = Y
    STANDBY_REPLAY_ONLY_WINDOW_ACTIVE = N

Standby:

Database Member 0 -- Database RWGMXPRD -- Active Standby -- Up 0 days 00:00:45 -- Date 2017-11-16-15.11.43.439915

                            HADR_ROLE = STANDBY
                          REPLAY_TYPE = PHYSICAL
                        HADR_SYNCMODE = NEARSYNC
                           STANDBY_ID = 0
                        LOG_STREAM_ID = 0
                           HADR_STATE = PEER
                           HADR_FLAGS =
                  PRIMARY_MEMBER_HOST = db2-001.prd.rwg.eams.cloud
                     PRIMARY_INSTANCE = ctginst1
                       PRIMARY_MEMBER = 0
                  STANDBY_MEMBER_HOST = db2-002.prd.rwg.eams.cloud
                     STANDBY_INSTANCE = ctginst1
                       STANDBY_MEMBER = 0
                  HADR_CONNECT_STATUS = CONNECTED
             HADR_CONNECT_STATUS_TIME = 11/16/2017 15:11:10.640718 (1510845070)
          HEARTBEAT_INTERVAL(seconds) = 30
                     HEARTBEAT_MISSED = 0
                   HEARTBEAT_EXPECTED = 1
                HADR_TIMEOUT(seconds) = 120
        TIME_SINCE_LAST_RECV(seconds) = 3
             PEER_WAIT_LIMIT(seconds) = 0
           LOG_HADR_WAIT_CUR(seconds) = 0.000
    LOG_HADR_WAIT_RECENT_AVG(seconds) = 0.000511
   LOG_HADR_WAIT_ACCUMULATED(seconds) = 26241.010
                  LOG_HADR_WAIT_COUNT = 51273558
SOCK_SEND_BUF_REQUESTED,ACTUAL(bytes) = 0, 332800
SOCK_RECV_BUF_REQUESTED,ACTUAL(bytes) = 0, 235128
            PRIMARY_LOG_FILE,PAGE,POS = S0001606.LOG, 3130, 26866164529
            STANDBY_LOG_FILE,PAGE,POS = S0001606.LOG, 3130, 26866164529
                  HADR_LOG_GAP(bytes) = 0
     STANDBY_REPLAY_LOG_FILE,PAGE,POS = S0001606.LOG, 3130, 26866164529
       STANDBY_RECV_REPLAY_GAP(bytes) = 302
                     PRIMARY_LOG_TIME = 11/16/2017 15:11:40.000000 (1510845100)
                     STANDBY_LOG_TIME = 11/16/2017 15:11:40.000000 (1510845100)
              STANDBY_REPLAY_LOG_TIME = 11/16/2017 15:11:40.000000 (1510845100)
         STANDBY_RECV_BUF_SIZE(pages) = 2048
             STANDBY_RECV_BUF_PERCENT = 0
           STANDBY_SPOOL_LIMIT(pages) = 1048576
                STANDBY_SPOOL_PERCENT = 0
                   STANDBY_ERROR_TIME = NULL
                 PEER_WINDOW(seconds) = 0
             READS_ON_STANDBY_ENABLED = Y
    STANDBY_REPLAY_ONLY_WINDOW_ACTIVE = N



