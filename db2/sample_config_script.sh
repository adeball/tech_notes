
#!/usr/bin/env bash
#
# db2_datavaul_config.sh
#
# Based on https://confluence.eams-group.com/display/HD/Installing+DB2+on+Amazon+Linux
#

if [[ ${USER} != "db2inst1" ]] ; then
    echo
    echo "This script must be executed as the db2inst1 user"
    echo
fi

cd

db2start
db2 update dbm config using SVCENAME 50005 DEFERRED  ### Listeners, on port 50005
db2stop
db2set DB2COMM=tcpip
db2start

### Setup the database - 8 character limit ( :-( )

db2 create db 'datavaul' on /data/datavault ALIAS 'datavaul' using codeset UTF-8 territory US pagesize 32 K

db2 update db cfg for datavaul using SELF_TUNING_MEM ON
db2 update db cfg for datavaul using APPGROUP_MEM_SZ 16384 DEFERRED
db2 update db cfg for datavaul using APPLHEAPSZ 2048 AUTOMATIC DEFERRED
db2 update db cfg for datavaul using AUTO_MAINT ON DEFERRED
db2 update db cfg for datavaul using AUTO_TBL_MAINT ON DEFERRED
db2 update db cfg for datavaul using AUTO_RUNSTATS ON DEFERRED
db2 update db cfg for datavaul using AUTO_REORG ON DEFERRED
db2 update db cfg for datavaul using AUTO_DB_BACKUP ON DEFERRED
db2 update db cfg for datavaul using CATALOGCACHE_SZ 800 DEFERRED
db2 update db cfg for datavaul using CHNGPGS_THRESH 40 DEFERRED
db2 update db cfg for datavaul using DBHEAP AUTOMATIC
db2 update db cfg for datavaul using LOCKLIST AUTOMATIC DEFERRED
db2 update db cfg for datavaul using LOGBUFSZ 1024 DEFERRED
db2 update db cfg for datavaul using LOCKTIMEOUT 300 DEFERRED
db2 update db cfg for datavaul using LOGPRIMARY 20 DEFERRED
db2 update db cfg for datavaul using LOGSECOND 100 DEFERRED
db2 update db cfg for datavaul using LOGFILSIZ 8192 DEFERRED
db2 update db cfg for datavaul using SOFTMAX 1000 DEFERRED
db2 update db cfg for datavaul using MAXFILOP 61440 DEFERRED
db2 update db cfg for datavaul using PCKCACHESZ AUTOMATIC DEFERRED
db2 update db cfg for datavaul using STAT_HEAP_SZ AUTOMATIC DEFERRED
db2 update db cfg for datavaul using STMTHEAP AUTOMATIC DEFERRED
db2 update db cfg for datavaul using UTIL_HEAP_SZ 10000 DEFERRED
db2 update db cfg for datavaul using DATABASE_MEMORY AUTOMATIC DEFERRED
db2 update db cfg for datavaul using AUTO_STMT_STATS OFF DEFERRED
db2 update db cfg for datavaul using STMT_CONC LITERALS DEFERRED

### Update alerts
db2 update alert cfg for database on datavaul using db.db_backup_req SET THRESHOLDSCHECKED YES
db2 update alert cfg for database on datavaul using db.tb_reorg_req SET THRESHOLDSCHECKED YES
db2 update alert cfg for database on datavaul using db.tb_runstats_req SET THRESHOLDSCHECKED YES

### Update the config
db2 update dbm cfg using PRIV_MEM_THRESH 32767 DEFERRED
db2 update dbm cfg using KEEPFENCED NO DEFERRED
db2 update dbm cfg using NUMDB 2 DEFERRED
db2 update dbm cfg using RQRIOBLK 65535 DEFERRED
db2 update dbm cfg using HEALTH_MON OFF DEFERRED

### Update settings
db2set DB2_SKIPINSERTED=ON
db2set DB2_INLIST_TO_NLJN=YES
db2set DB2_MINIMIZE_LISTPREFETCH=Y
db2set DB2_EVALUNCOMMITTED=YES
db2set DB2_FMP_COMM_HEAPSZ=65536
db2set DB2_SKIPDELETED=ON
db2set DB2_USE_ALTERNATE_PAGE_CLEANING=ON

### Restart and reconnect
db2stop force
db2start
db2 connect to 'datavaul'

### Setup tablespaces - 
db2 CREATE BUFFERPOOL MAXBUFPOOL IMMEDIATE SIZE 4096 AUTOMATIC PAGESIZE 32 K
db2 CREATE REGULAR TABLESPACE MAXDATA PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE INITIALSIZE 2500 M BUFFERPOOL MAXBUFPOOL
db2 CREATE TEMPORARY TABLESPACE MAXTEMP PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE BUFFERPOOL MAXBUFPOOL
db2 CREATE REGULAR TABLESPACE MAXINDEX PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE INITIALSIZE 2500 M BUFFERPOOL MAXBUFPOOL
db2 GRANT USE OF TABLESPACE MAXDATA TO USER DATAVAUL
db2 create schema datavaul authorization datavaul
db2 GRANT DBADM,CREATETAB,BINDADD,CONNECT,CREATE_NOT_FENCED_ROUTINE,IMPLICIT_SCHEMA, LOAD,CREATE_EXTERNAL_ROUTINE,QUIESCE_CONNECT,SECADM ON DATABASE TO USER DATAVAUL
db2 GRANT USE OF TABLESPACE MAXDATA TO USER DATAVAUL
db2 GRANT CREATEIN,DROPIN,ALTERIN ON SCHEMA DATAVAUL TO USER DATAVAUL

db2 connect reset


