
# SIZE=space used, CAPACITY=space allocated in bytes

$ db2 connect to d01

   Database Connection Information

 Database server        = DB2/LINUXX8664 9.7.4
 SQL authorization ID   = DB2INST1
 Local database alias   = D01

$ db2 "call get_dbsize_info(?,?,?,-1)"

  Value of output parameters
  --------------------------
  Parameter Name  : SNAPSHOTTIMESTAMP
  Parameter Value : 2012-06-18-10.34.39.907630

  Parameter Name  : DATABASESIZE
  Parameter Value : 1157869568

  Parameter Name  : DATABASECAPACITY
  Parameter Value : 1307672571

  Return Status = 0


# List containers

db2pd -d d01 -tablespaces



