* Database users are defined by the OS (i.e credentials are OS creds, however that is done).
* Usernames and DB names have an 8 character limit
* Windows/Linux DBs are not compatible.  Check db2move/db2look: https://technomagus.wordpress.com/database/ibm-db2/transfer-db2-database-from-linux-to-windows/


#Remote database

To connect to a remote database, you need to add it to the database catalog, specifying the name of the remote node.
The remote node name is not looked up outside DB2, it needs first adding to the node directory.

e.g.

* db2 
* list node directory (to see what's already created)
* catalog tcpip node myremoteservername remote 10.0.0.1 server 50005  (where myremoteservername is an arbitrary name of your choice, 10.0.0.1 is the remote server IP, and 50005 is the remote listener tcp port)
* catalog database myremotedb as myremotedbalias at node myremoteservername


#Copy to/from Linux to Windows

A backup/restore won't work due to the endian difference (what, are we in the 1970s!?) - you can use db2move/db2look to export/import instead.

e.g.

db2move GTRMXTST export -aw -l lobs -sn MAXIMO -u maximo -p mypasswd
db2look -d GTRMXTST -a -e -x -i maximo -o GTRMXTST_maximo.ddl -w mypasswd

db2move newdbname import -io replace_create -l lobs -u maximo -p mypasswd
Edit the ddl file used in the next statement, and change the database name near the top to 'newdbname' if necessary
NB: You can also add the password with e.g. "user myusername using mypassword"
db2 -tvf GTRMXTST_maximo.ddl


#Database federation

https://www.ibm.com/developerworks/community/blogs/db2luwtechsupport/entry/how_to_setup_federation_between_two_db2_luw_databases2?lang=en


db2 connect to datavaul
db2 update dbm cfg using federated yes

db2stop [force if needed]
db2start

db2 get dbm cfg | grep -i federated


db2level (to see versions)
db2 

db2 => create server maximo_db type db2/udb version 10.5.4 wrapper drda authorization "maximo" password "thedbpassword" options(add DBNAME 'maximo')
DB20000I  The SQL command completed successfully.

db2 => create user mapping for datavaul server maximo_db options(remote_authid 'maximo', remote_password 'thedbpassword')
DB20000I  The SQL command completed successfully.


### Drop a database

CONNECT TO XYZ;
UNQUIESCE DATABASE;
QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS;
UNQUIESCE DATABASE;
CONNECT RESET;
deactivate db XYZ;
DROP DATABASE XYZ;

### Backup

db2 backup database GTRMXPRD to E:\backup\GTRMXTST compress


### After upgrades

[8:04 AM] Giles Klech: After schema recreations, database drops and recreations it was the recreation of the database plus the execution of the 'db2rbind ${DBNAME} -l bind.log all' command that resolved the (extremely painful) issue.
[8:16 AM] Adrian Ball: Good job - how the heck did you work that out?
[8:18 AM] Adrian Ball: I'm looking at "db2 bind" and it's talking mainframes & cobol!
[8:18 AM] Adrian Ball: Ye anciente technologie
[8:19 AM] Giles Klech: After recreating the database and everything still failed due to the deadlock / timeout issue I (in desperation) just googled the reason code with the error and found a page that stated that after patching DB2 it's recommended to execute that statement. I gave it a shot after the failure and 25 packages failed to bind so I dropped the database again, recreated it manually and executed the command. This time it worked so I kicked off the database update again and went to bed. Came down this morning and it had finally completed successfully.
[8:20 AM] Giles Klech: Yeah, I don't honestly know what it does. There's a similar command in Oracle that you run to ensure all system stored procedures are compiled post migration / upgrade.


### List schemas

select schemaname from syscat.schemata


### db2 v10.5 db2start -> SQL1042C  An unexpected system error occurred.  SQLSTATE=58004

https://www.ibm.com/support/knowledgecenter/SSEPGG_11.1.0/com.ibm.db2.luw.messages.sql.doc/doc/msql01042c.html
(Basically, use db2iupdt to fix)

