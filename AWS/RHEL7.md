# RHEL7 (Red Hat Enterprise Linux 7) support in AWS

Annoyingly, RHEL7 AMIs do not come pre-rolled with the AWS CLI utilities, unlike the Amazon Linux AMIs.
Because it's sometimes necessary to use RHEL (for product support in production environments etc), I had to find the way to add this to RHEL.
 And here it is:-

      UserData:
        "Fn::Base64":
          !Sub
            - |+
              #!/bin/bash -v
              
              ### AWS CLI
              curl https://bootstrap.pypa.io/get-pip.py | python ; pip install awscli pystache argparse python-daemon requests
              cd /opt ; curl -O https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz
              tar xvfz aws-cfn-bootstrap-latest.tar.gz
              ln -s aws*[0-9] aws
              cd aws ; python setup.py build ; python setup.py install ; chmod a+x /opt/aws/bin/*
              
              ### CloudFormation 
              ln -s /usr/init/redhat/cfn-hup /etc/init.d/cfn-hup ; chmod 775 /usr/init/redhat/cfn-hup
              echo /opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource EC2 --configsets ${ConfSets} --region ${AWS::Region}
              /opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource EC2 --configsets ${ConfSets} --region ${AWS::Region}
              
Use this in the UserData section of a CF YAML template (or put it in wherever your bootstrap scripts get run).  
Obviously ConfSets will need modifying as required...  If you only want the AWS CLI, just use that section and forget the CF lines.
