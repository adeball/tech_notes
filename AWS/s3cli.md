 1005  aws s3api get-bucket-policy --bucket eams-cloud-backup --output=text | jq . > policy.json
 1012  vi policy.json 
 1013  aws s3api put-bucket-policy --bucket eams-cloud-backup --policy file://policy.json 
 1014  aws s3api get-bucket-policy --bucket eams-cloud-backup --output=text | jq .


### Permission to write to cross account buckets.

See the eams-cloud-puppet/scripts/setup_buckets.sh script for policy application examples.
Also from another account, you can use, e.g. :
aws s3 cp localfile s3://bucket/remotefile --grants=full=id=verylongstring

Where the ID (verylongstring) is the "account identifier" of an account which has permissions on the bucket.  This can be obtained on the CLI by using "aws s3api list-buckets" when set up as that account. (Or see https://docs.aws.amazon.com/general/latest/gr/acct-identifiers.html for more information).


