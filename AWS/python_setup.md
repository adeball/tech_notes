
Precis of https://aws.amazon.com/articles/3998

* apt-get install python-pip -y
* pip install -U boto
* Create ~/.boto as follows

```
[Credentials]
aws_access_key_id = ...
aws_secret_access_key = ...
```

* chmod 400 ~/.boto
NB: You can use /etc/boto.cfg, but that's even more dangerous...  Probably better to use a designated EC2 management server with the right role, but this works for testing.


