#How to set up an AWS hardware VPN with a TP-LINK Archer VR900

Your mileage may vary with other routers, but this works for me - I'm in the UK using Vodafone broadband.

## AWS configuration

All AWS configuration is within the VPC section.

* Create a customer gateway (NB: I found this term confusing to start with - it's not an entity on AWS as such, 
more a pointer/definition of your router to be used by AWS)
    * Name tag: Whatever you want, e.g. MyHomeRouter
    * Routing: static
    * IP address: the external facing IP address of your router
    
* Create a virtual private gateway
    * Name tag: Whatever you want, e.g. MyAWS-VPG
    
* Attach the virtual private gateway
    * Select it, and press the 'attach to VPC' button
    * Select the VPC you want to route to/from
    
* Create a VPN connection
    * Name tag: e.g. MyHome-VPN
    * Virtual Private Gateway: The one you just created (MyAWS-VPG)
    * Customer Gateway: Existing, the one you just created (MyHomeRouter)
    * Routing options: Static
    * Static IP Prefixes: the CIDR notation for the hosts on your home network (that you want AWS to route to), e.g. 192.168.1.0/24
    You can add several, or limit to individual hosts etc.
    
* Create route(s)
    * Go to "Route Tables"
    * Select the route table for the subnets you want to route to/from
    * In the bottom panel, select the "Routes" tab
    * You will probably see a "local" route, and possibly a 0.0.0.0/0 route (pointing to igw-xxxxxxx, i.e. an Internet Gateway)
    * Add a new route 
        * Press "Edit"
        * Press "Add another route"
        * Under destination, add your home network in CIDR notation - e.g. 192.168.1.0/24
        * Under target, select the virtual private gateway you just created (MyAWS-VPG)
        * Press "Save"
        * Repeat as required for additional route tables (e.g. you may have a private and public route table)
        
* Obtain the AWS VPN details (you'll use these to configure the VR900)
    * Go back to the "VPN Connections" page
    * Select the VPN connection you just created, and press "Download configuration"
    * Change the Vendor to "Generic"
    * Press "yes, download" to obtain a text dump of the config
    * For now, just use the "IPSec Tunnel #1" section, and note:
        * From section #1: The Pre-Shared key
        * From section #3: Under "Outside IP Addresses", the Virtual Private Gateway IP (e.g. 34.123.45.67)
        
That's the AWS side done for now...

## TP-LINK configuration

NB: Settings here are for complete network-network links, you may want to adjust...

* Login to your router
* Select the 'advanced' tab
* Select Network/IPSec VPN from the list on the left
* Enable "Dead Peer Detection"
* Select Add
    * IPSec Connection Name: AWS VPN Link #1 (or anything you like)
    * Remote IPSec Gateway Address(URL): 34.123.45.67 (the address you obtained from the AWS config dump in the "Outside
     IP Addresses" section)
    * Tunnel access from local IP addresses: Subnet
        * IP Address for VPN: 192.168.1.0 (your home network address)
        * IP Subnetmask: 255.255.255.0 (your home network netmask)
    * Tunnel access from remote IP addresses: Subnet
        * IP Address for VPN: 10.1.0.0 (This is the network address of the AWS VPC you're connecting to)
        * IP Subnetmask: 255.255.0.0 (The netmask for the entire VPC - if appropriate, you may want to adjust)
    * Key Exchange Method: Auto(IKE)
    * Authentication Method: Pre-Shared Key
    * Pre-Shared Key: Paste in the Pre-Shared Key you obtained from the AWS config dump
    * Perfect Forward Secrecy
* Now click the "advanced" option
    * Under ==Phase 1==
        * Mode: Main
        * My Identifier Type: Local Wan IP
        * Remote Identifier Type: Remote Wan IP
        * Encryption Algorithm: AES - 128
        * Integrity Algorithm: SHA1
        * Diffie-Hellman Group for Key Exchange: 1024bit
        * Key Life Time: 28800
    * Under ==Phase 2==
        * Encryption Algorithm: AES - 128
        * Integrity Algorithm: SHA1
        * Diffie-Hellman Group for Key Exchange: 1024bit
        * Key Life Time: 3600
        
* Hit OK

## Check

If everything worked, you should see that the link status is "up" on the TP-LINK

* On AWS, go to "VPN connections" and select your VPN.
* At the bottom of the screen, select the "Tunnel details" tab, you should see that "Tunnel 1" is UP

You should now be able to connect over IPSec from your internal network to Amazon and vice-versa, subject to firewall 
rules and security groups.  Once you're happy with the configuration, you can go ahead and add the second VPN (under IPSec Tunnel #2
in the config file - different IP and Pre-Shared Key to #1, the rest is the same), this will add resilience at the AWS end of the
connection.
