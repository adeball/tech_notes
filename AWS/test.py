#!/usr/bin/python

import boto
s3=boto.connect_s3()
bucket=s3.create_bucket('ux1_adetest')
bucket.set_acl('public-read')
key=bucket.new_key('examples/test.py')
key.set_contents_from_filename('/users/ade/tech_notes/aws/test.py')
key.set_acl('public-read')

