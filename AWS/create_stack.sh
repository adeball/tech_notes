#!/bin/bash

### Sample command line to create a new stack from a working cloudformation template

aws cloudformation create-stack --stack-name "AdeStack2" --template-body file:///users/ade/tech_notes/AWS/LoadBalancedAutoscaledWebServersInVPC.json --parameters ParameterKey=KeyName,ParameterValue="ux1-EC2-key",UsePreviousValue=false
