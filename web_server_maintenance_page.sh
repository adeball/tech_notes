#!/bin/bash

service="cryoSPARC"
period="2021-05-18 14:00-17:00"
reason="Upgrade to v3.2.0"
reference="ServiceNow REQTSK0422682"
port=39000

msg="HTTP/1.1 200 OK\n\n
<h1>The $service service on $(hostname):$port is down for maintenance</h1>    
Date/time of outage: $period<br>
Reason: $reason<br>
Please see: $reference, for details."

echo "Now reporting:"
echo $msg
echo on $(hostname):$port
echo

nc -lk -p $port -c "echo -e \"$msg\""
