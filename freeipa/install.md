
#Install packages

* Install CentOS (or RHEL) - CentOS 6.8 used below
* Edit /etc/sysconfig/network to set hostname to FQDN (ipa.hp5.co.uk)
* Edit /etc/sysconfig/network-scripts/ifcfg-eth0 to set a static address, add the default gateway
* Edit /etc/resolv.conf to point to existing DNS (for software installation)
* yum install -y ipa-server (accept warnings about GPG keys)
* Wait... takes 10 or 15 minutes

#Configure
ipa-server-install
* Server host name: ipa.hp5.co.uk
* Domain: hp5.co.uk
* Realm: HP5.CO.UK
* Enter a directory manager password, and admin password
* Wait... takes 10 or 15 minutes again

#Add hosts and users
kinit admin

## Users
e.g.
echo "initialpassword" | ipa ade --first=Ade --last=Ball --homedir=/users/ade --password --shell=/bin/bash 

kinit ade (to change password)
