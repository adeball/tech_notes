
To make scrolling go the same way as on a Mac, AKA 'natural scrolling'


#Windows


Instructions here:

http://superuser.com/questions/310681/inverting-direction-of-mouse-scroll-wheel

Basically (this is Win 7)
* Find the ID of mouse 
    * control panel/mouse/hardware/select the correct mouse 
    * properties
    * details
    * Select "hardware IDs" from the dropdown
    * Leave this up
* Run regedit
    * Go to HKLM:\SYSTEM\CurrentControlSet\Enum\HID\*\*\Device Parameters (where *\* includes your device ID)
    * Change FlipFlopWheel to '1'
* Unplug/replug mouse

#Linux

## Xwindows
xmodmap -e "pointer = 1 2 3 5 4 7 6 8 9 10 11 12"  (just put the numbers in ascending order to set back to normal)

## Firefox
set mousewheel.default.delta_multiplier_y to negative, e.g. -100 
