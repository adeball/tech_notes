
# Old client to new server

Modify sshd_config on new server to accept old stuff, e.g.

Add hmac-md5-96 to MACs line
Add diffie-hellman-group1-sha1 to KexAlgorithms line

# New client to old server

e.g.
ssh -oKexAlgorithms=+diffie-hellman-group1-sha1 -m hmac-md5-96 -o PubkeyAcceptedKeyTypes=+ssh-rsa -o HostKeyAlgorithms=+ssh-rsa oldserver
(Or add MAC, KexAlgorithms etc to ~/.ssh/config)

