Ensure these are set in /etc/ssh/sshd_config

X11Forwarding yes
X11DisplayOffset 10
X11UseLocalhost no   ### This is the non-obvious one...

Then stopsrc -s sshd;startsrc -s sshd 
