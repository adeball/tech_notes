set nocompatible
syntax enable
set guifont=Lucida_Console:h8:cANSI
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
