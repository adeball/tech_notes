
# IntelliJ IDEA notes

## After install

1. Use darcula, you know it makes sense
1. Add plugins for puppet, bash, IdeaVIM, markdown (use the Jetbrains one)
1. Set up long line wrapping: View / Active Editor / Use Soft Wraps (need this for each file type as it crops up)
1. Add eyaml: Preferences / Editor / File Types: YAML, add \*.eyaml
1. Add \_\*table\* as a bash type (so that commented lines are shown - it's just nicer)

