# Problem

```
tar cvf whatever.tar somewhere
tar tvf whatever.tar
```
results in:

```
file
._file
```

etc

i.e. The extended attributes are included in the archive

# Answer
Set
export COPYFILE_DISABLE=true
Before running tar
