From: http://tech.anoj.net/2013/01/fixing-unetbootin-on-mac-os-x-to-create.html

The important part is copying the syslinux mbr.bin
Without that, you are doomed...

Precis of instructions

Run Disk Utility - format as FAT, with MBR

diskutil list to get the right dev name

e.g. diskutil unmountDisk /dev/rdisk2


Make the partition active and unmount the disk

sudo fdisk -e /dev/rdisk2
print
f 1
write
print
exit

diskutil unmountDisk /dev/rdisk2



Now, download the latest syslinux.zip, and extract and locate the mbr.bin
Go back to the extracted folder in the terminal, and install mbr.bin


sudo dd conv=notrunc bs=440 count=1 if=mbr.bin of=/dev/rdisk2

Use Unetbootin to install the ISO...
