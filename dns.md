
Top level domains safe to use internally (will never be registered as TLDs): 
.home, .corp, .mail


# Linux DNS timeout stuff

Add e.g. RES_OPTIONS="timeout:1 attempts:1 rotate" to /etc/sysconfig/network to autorotate failing DNS
(Or add "options timeout:1 attempts:1 rotate" to /etc/resolv.conf if it is not auto-generated)

On subsequent lookups, it goes for a new DNS, not the first one


## RHEL 8 (may work on earlier versions, but not tested)

If NetworkManager is installed and re-writing /etc/resolv.conf, you can prevent this by adding your settings in a new file (e.g. /etc/resolv.conf.custom), then removing /etc/resolv.conf and then symlinking it to the new file.
