### Ade's .bashrc

### Local stuff for specific client environments
[[ -f ~/.bashrc_local ]] && . ~/.bashrc_local

### In case we're run from gnome shell etc
PS1="[\u@\h:\w]$ "

umask 027

