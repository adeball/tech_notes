
## Password reset (inc admin user)

sudo -u <web user> php docroot/admin/cli/reset_password.php 


## Disable site (for maintenance)

if [[ -f /var/www/moodle/docroot/admin/cli/maintenance.php ]] ; then
        touch /www/moodle/requests/STOP_CRONS
        systemctl stop crond
        sudo -u apache php /var/www/moodle/docroot/admin/cli/maintenance.php --enable ### MOODLE
fi

if [[ -f /www/mahara/docroot/admin/cli/close_site.php ]] ; then 
        systemctl stop crond
        sudo -u apache php /www/mahara/docroot/admin/cli/close_site.php --close       ### MAHARA
fi



## Enable site (after maintenance)

if [[ -f /var/www/moodle/docroot/admin/cli/maintenance.php ]] ; then
        sudo -u apache php /var/www/moodle/docroot/admin/cli/maintenance.php --disable ### MOODLE
        rm -f /www/moodle/requests/STOP_CRONS
fi

if [[ -f /www/mahara/docroot/admin/cli/close_site.php ]] ; then 
        sudo -u apache php /www/mahara/docroot/admin/cli/close_site.php --open         ### MAHARA
fi


