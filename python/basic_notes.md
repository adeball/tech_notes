
# Basics

* Usual #!/usr/bin/python (or whatever the path is) at start of scripts
* Most envs have IDLE as a dev env, invoke with 'idle'
* Standard ext is .py (.pyc is compiled)

* Variables - start with _ or alpha character

# Help

* Within python: help()
   * Within help: modules, keywords, topics 
      * modules: lists all python modules
      * keywords: builtin keywords (for, if etc)
      * topics: useful groupings

* At the shell: pydoc

# Intellij IDE
Free and works really well.
NB you need to install the jetstream python plugin
Useful shortcuts:-
* cmd-/ : toggle comments over selected lines (i.e. useful for commenting code-blocks)
* ctrl-shift-R : Run the code
* alt-cmd-L : format the code (pretty print!)

#Calc
Usual + - / * %(mod)
NB a / b even if a & b are ints, will return a float
Also // (divide and return an integer)

#Strings
* Can use stringvar[n] to get nth char of stringvar (n starts at 0)
* Can also use stringvar[-n] (so -1 is the last character)
* Plus, can use a slice (e.g stringvar[3:14] returns 14 characters starting at character 3 (which is the 4th character as we are counting from 0)
* Can also use stringvar[:6] (first 6 chars, same as stringvar[0:6]), and stringvar[6:] which is all characters from char 6 (i.e. the 7th character)
* Can use a third parameter as a step to return every e.g. 2nd character

* Can use e.g. print("Hello " * 3) to get "Hello Hello Hello "

* Use 'in' to check for substring presence - e.g. print("day" in "today") returns true

* Concatenating strings in a loop is inefficient - as a new string is created each time.  Use join() instead.

##Replacement fields - optional 2nd parameter after the colon is field width
age=24
print("my age is {0} years".format(age))
Or: print("There are {0} days in ".format(31,"January","March","May","July","August","October","December")

##Deprecated (formatting strings)
print("My age is %d %s" % (age, "years"))

### Using both versions:-

for i in range(1, 12):
    print("%2d squared is %3d, cubed is %4d" % (i, i ** 2, i ** 3))

for i in range(1, 12):
    print("{0:2} squared is {1:3}, cubed is {2:4}".format(i, i ** 2, i ** 3))

####Left formatted, use "<"
for i in range(1, 12):
    print("{0:<2} squared is {1:<3}, cubed is {2:<4}".format(i, i ** 2, i ** 3))

####For precision use . - e.g.
print("Pi is approx {0:12.50}".format(22/7))  = twelve chars for the LHS, 50 point precision printed

#### Can use fields out of order multiple times, or infer order with {} only
e.g. print("My age is {} {}" % (age, "years")) or
or print("My age is {0} {1}, yep that's {0} {1}" % (age, "years"))

#Bool
No boolean type, but static 'True' and 'False' variables exist.

#Print, use end='' if you don't want the auto newlines

Can use e.g. 
for char in 'dkwedjhwef':
to get each character in a string

* Weird 'else' at the end of a for or a while loop:  Executed if the loop completed (i.e. no "break" used)

* Line continuation - backslah can be used, but style guides prefer split strings by enclosing in brackets


#Sequence types
* List, range, tuple (also: bytes, bytearray & memoryview)

#Sorting
sequence.sort() - actually modifies sequence, and returns "None"
sorted(sequence) - returns the sorted sequence, without modifying the original sequence

#Assigning a list
even=[2,4,6,8]
even2=even
even2.sort(reverse=True)
[8,6,4,2]
(i.e. it's working on the same pointer)

* Can test if two lists are the same (pointer) with 'is'.  e.g. if list1 is list2, rather than if list1 == list2, which tests for content equality.

#Iterable objects
* strings, lists
for loop will iterate through each char in a string or each item in a list
* Or can use the iter function (e.g. myiter=iter(string), then use e.g. print(next(myiter)) to get the first char/element.  Then check for an error, which you get after the last item.

* "for char in string" implicitly adds the iter function, i.e. it's the same as for "char in iter(string)"

* Can use the index() function to get the indexth item from a range, list, string 
#Negative steps
* string="abcdef"
* print(string[::-1]) reverses it to fedcba

#Tuples
* Brackets not needed unless ambiguous, but it's probably best practice to use them anyway
* Can assign parts of a tuple in one go i.e. unpack it.  e.g. album=("In Search of Space","Hawkwind",1972)  then title,artist,year=album

#Format to print binary,hex,octal
print("{0} in binary is {0:b}, octal {0:o}, hex {0:h}".format(i))


# Dictionaries

fruit = {"orange": "Sweet, orange coloured citrus", "apple": "Green, good in pies", "lemon": "Yellow citrus, sour"}
print(fruit["orange"])
fruit["pear"] = "Like an apple"  ## Add an entry to a dictionary (or replace if it is already there)
del fruit["apple"] ## Delete one item
del fruit ## Remove entire dictionary
fruit.clear() ## Empties dictionary


Use e.g. fruit.get("apple") rather than fruit["apple"] - returns "None" rather than an error
You can also use e.g. fruit.get(dict_key,"there is no such fruit as" + dict_key + "in the dictionary") ## Second item is the default value returned if no match

Check for key: if dict_key in fruit:

list(fruit.keys()) returns a list of the keys - so you can use sort() or sorted() if you want them in order e.g. sorted(fruit.keys()) (for sort() you need to convert to a list first - e.g. skeys=list(fruit.keys()) skeys.sort()
or for f in fruit: for unsorted

NB: fruit.keys() returns a "view object", which is updated when the dictionary is updated - i.e. assign it to a variable and that variable is updated if the dictionary changes.

for val in fruit.values():  Gives the values, but is less efficient than using the keys

You can also use fruit.items(), which returns the keys and values together

You can use the tuple function to create tuples from a dictionary, and reverse this to create a dictionary from a tuple using the dict function.





