#!/usr/bin/python3
#
# Simple stuff- just for syntax and a couple of neat tricks
# Redundant use of a list of lists and list of dictionaries, 
# to show both options
#
# Obviously this should be in a data structure, and accessed by nice functions,
# but I don't know python well enough yet
#

import json

def separator(ndashes=30):
	print('\n', '-' * ndashes, '\n' )	# Neat way of printing the same char a few times

pfile=open('/etc/passwd','r') 			# w=write, a=append, r+=read/write

keys=['name','passwd','uid','gid','gecos','dir','shell']

llist=[]
dlist=[]

for line in pfile:
	l=line.rstrip('\r\n').split(":")	# List of each element of line in passwd file
	llist.append(l)				# List of lists

	d=dict(zip(['name','passwd','uid','gid','gecos','dir','shell'],l)) # Creates a dictionary with keys defined and values read
	dlist.append(d)


print(llist)					# Just to see it

separator()

print(dlist)

separator(60)

print(json.dumps(llist,indent=4))		# Python lists are already in JSON format, but this 
print(json.dumps(dlist,indent=4))		# makes it really easy to read :-)

separator()

### Then we can e.g. get all names from the passwd file
for d in dlist:	
	print(d.get('name'))

pfile.close					# Not necessary, but we should do it
