
### Stuff we need to know at every site/client

1. Access - user login(s)
1. Key contacts
1. Remote Access
1. Directory Services - Users, groups, roles, servers etc
1. Email - both client and SMTP gateways etc
1. Calendar
1. Client build
1. Server build
1. Password management
1. Knowledge sharing (wiki etc)
1. Change management
1. Problem management
1. Incident management
1. Performance monitoring
1. Configuration management
1. IM
1. Project planning/review
1. Book-keeping (project codes/cost codes etc)
1. Document management
1. Client updates
1. Server updates
1. Software licence management
1. Document publication (web)
1. User management (uid/gid etc)
1. Storage (user personal, group sharing, company global etc)
1. Host name/IP address management
1. Network configuration/management
1. External Web Presence
1. Staff scheduling (who is in/out/where/holidays etc)
1. Server naming
1. Environments (test, dev, pre, prod etc)
1. Electricals (PAT testing etc)
1. Backup / restore service
1. Service monitoring (problems)
1. Server room access
1. Where does work come from (Remedy/ServiceNow/JIRA etc)
1. Training resources
1. Time recording
