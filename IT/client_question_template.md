
### Stuff we need to know at every site/client

1. Directory Services - Users, groups, roles, servers etc
2. Email
3. Calendar
4. Client build
5. Server build
6. Password management
7. Knowledge sharing (wiki etc)
8. Change management
9. Problem management
10. Incident management
11. Performance monitoring
12. Configuration management
13. IM
14. Project planning/review
15. Book-keeping (project codes/cost codes etc)
16. Document management
17. Client updates
18. Server updates
19. Software licence management
20. Document publication (web)
21. User management (uid/gid etc)
22. Storage (user personal, group sharing, company global etc)
23. Host name/IP address management
24. Network configuration/management
25. External Web Presence
26. Staff scheduling (who is in/out/where/holidays etc)
27. Server naming
28. Environments (test, dev, pre, prod etc)
29. Electricals (PAT testing etc)
30. Backup / restore service
31. Service monitoring (problems)
