Version: 3.5
Should be set up with proper repos - use gpfs-base to install the main packages, then gpfs-updates
Packages gpfs.base gpfs.gpl gpfs,docs gpfs.msg_en_US
Manual installation / joining an existing cluster
Install the 4 rpms as listed above, then the updates.
(On SLES, use zypper install *rpm for local packages if the repo is not working)
 
Compile the kernel modules e.g.
547 2016-10-11 10:05:37 cd /usr/lpp/mmfs/src
548 2016-10-11 10:05:41 make Autoconfig
549 2016-10-11 10:05:47 ls -lart
550 2016-10-11 10:05:56 make World
551 2016-10-11 10:06:17 make InstallImages
552 2016-10-11 10:06:40 make rpm (or make deb)
 
Install the package you just made
553 2016-10-11 10:06:57 zypper install /usr/src/packages/RPMS/x86_64/gpfs.gplbin-3.0.76-0.11-default-3.5.0-24.x86_64.rpm

 Ensure that all network interfaces are talking to other nodes in the cluster, and that you can ssh with keys between them

Copy /var/mmfs/gen/mmsdrfs from a working node.
 
 Start it up (see commands)
# Useful commands
mmgetstate -aL	
Shows the state of cluster nodes, e.g.
host root ~ # mmgetstate -aL

 
 Node number  Node name       Quorum  Nodes up  Total nodes  GPFS state  Remarks   
------------------------------------------------------------------------------------
       1      stor_01           1*        2         17       active      quorum node
       2      stor_02           1*        2         17       active      quorum node
       3      comp08_01-ib       0        0         17       unknown    
       4      comp08_02-ib      1*        2         17       active     
       5      comp08_03-ib      1*        2         17       active     
       7      comp09_01-ib      1*        2         17       active     
       8      comp09_02-ib      1*        2         17       active     
       9      comp09_03-ib      1*        2         17       active     
      10      comp09_04-ib      1*        2         17       active     
      11      hn_01-ib          1*        2         17       active     
      12      ln_01-ib          1*        2         17       active     
      13      xcat_01-ib        1*        2         17       active     
      14      viz_01-ib          0        0         17       unknown    
      15      viz_02-ib         1*        2         17       active     
      16      sn_01-ib          1*        2         17       active     
      17      comp08_04-ib      1*        2         17       active     
      18      viz_03-ib          0        0         17       unknown

mmstartup -a	Starts services

mmdelnode -f	Run on node. Removes that node (i.e. itself), removes config files

mmaddnode-N viz_01-ib	Run on a working node, adds a new node (might also need mmchlicense)

#Paths
 
/usr/lpp/mmfs/bin: commands
/var/mmfs: config
/var/adm/ras: logs
/etc/init.d/gpfs: start script

#Stuff to look for
mmfsd must be running for GPFS to work
Check if you can load the mmfslinux kernel module
