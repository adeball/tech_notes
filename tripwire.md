#Reconfiguration

The configuration is held in /etc/tripwire/twcfg.txt, but must be copied to the binary version of the file when changed.  Use "man twadmin" to see the options.  Update the binary copy as follows:-

cd /etc/tripwire
twadmin -m f -c tw.cfg > twcfg.txt
<Edit twcfg.txt>
twadmin -m F -c tw.cfg --site-keyfile site.key twcfg.txt
rm twcfg.txt

A similar process is used to update the policy file:

cd /etc/tripwire
twadmin -m p -p tw.pol > twpol.txt
<Edit twpol.txt>
twadmin -m P -p tw.pol --site-keyfile site.key twpol.txt
rm twpol.txt

 
#Remove violations from the database

Where tripwire has spotted changes, but they are ones you have dealt with already.  You can reinitialise the database entirely with tripwire --init, but it's probably better to update it as follows:-

cd /var/lib/tripwire/report ;  tripwire -m u -a -r $(ls -rt|tail -1)

This command essential runs tripwire in "update" mode (-m u), and accepts all changes in the latest report file (-r filename).

